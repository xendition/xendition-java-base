package com.michael.demos.base.reflect.annocation.ioc.annocation;

import java.lang.annotation.*;

/**
 * 自动装配
 *
 * @author Michael
 * @create 2020-08-05 17:03
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Autowired {
}
