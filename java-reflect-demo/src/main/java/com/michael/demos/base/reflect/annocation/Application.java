package com.michael.demos.base.reflect.annocation;

import com.michael.demos.base.reflect.annocation.ioc.ApplicationContext;
import com.michael.demos.base.reflect.annocation.ioc.client.AppleService;

/**
 * 类功能描述:
 * <pre>
 *   反射 + 注解 - DEMO
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/5 11:52
 */
public class Application {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = new ApplicationContext();
        AppleService bean = context.getBean(AppleService.class);
        bean.test();
    }
}
