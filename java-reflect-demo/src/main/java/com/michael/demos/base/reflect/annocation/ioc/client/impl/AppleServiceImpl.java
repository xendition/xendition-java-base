package com.michael.demos.base.reflect.annocation.ioc.client.impl;

import com.michael.demos.base.reflect.annocation.ioc.annocation.Autowired;
import com.michael.demos.base.reflect.annocation.ioc.annocation.Component;
import com.michael.demos.base.reflect.annocation.ioc.client.AppleService;
import com.michael.demos.base.reflect.annocation.ioc.client.BasketService;
import com.michael.demos.base.reflect.annocation.ioc.client.BizService;

/**
 * 类功能描述:
 * <pre>
 *   示范类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/6 13:40
 */
@Component
public class AppleServiceImpl implements AppleService {

    @Autowired
    private BasketService basketService;
    @Autowired
    private BizService bizService;

    @Override
    public void test() {
        System.out.println("AppleServiceImpl:test");
        bizService.test();
        basketService.test();
    }
}
