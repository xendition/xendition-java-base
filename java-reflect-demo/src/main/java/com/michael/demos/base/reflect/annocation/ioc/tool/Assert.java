package com.michael.demos.base.reflect.annocation.ioc.tool;

/**
 * 类功能描述:
 * <pre>
 *   简单断言处理类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/6 14:25
 */
public class Assert {

    public static void isTrue(boolean expression, String errorMsg) {
        if (!expression) {
            throw new AssertException(errorMsg);
        }
    }

    public static void notNull(Object object, String errorMsg) {
        isTrue(object != null, errorMsg);
    }
}
