package com.michael.demos.base.reflect.annocation.ioc.client;

/**
 * 类功能描述:
 * <pre>
 *   示范类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/6 13:51
 */
public interface BizService {
    void test();
}
