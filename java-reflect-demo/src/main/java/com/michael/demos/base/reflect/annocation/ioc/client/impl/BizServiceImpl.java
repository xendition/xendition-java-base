package com.michael.demos.base.reflect.annocation.ioc.client.impl;

import com.michael.demos.base.reflect.annocation.ioc.annocation.Component;
import com.michael.demos.base.reflect.annocation.ioc.client.BizService;

/**
 * 类功能描述:
 * <pre>
 *   示范类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/6 13:51
 */
@Component
public class BizServiceImpl implements BizService {
    @Override
    public void test() {
        System.out.println("BizServiceImpl:test()");
    }
}
