package com.michael.demos.base.reflect.annocation.ioc.client;

import com.michael.demos.base.reflect.annocation.ioc.annocation.Component;

/**
 * 类功能描述:
 * <pre>
 *   示范类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/5 17:10
 */
@Component
public class FruitService {

    private String name;

    public void test() {
        System.out.println("FruitService:test()");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
