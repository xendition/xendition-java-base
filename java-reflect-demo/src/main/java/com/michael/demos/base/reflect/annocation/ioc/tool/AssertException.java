package com.michael.demos.base.reflect.annocation.ioc.tool;

/**
 * 类功能描述:
 * <pre>
 *   自定义异常
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/6 14:27
 */
public class AssertException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AssertException(String message) {
        super(message);
    }
}
