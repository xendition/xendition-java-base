package com.michael.demos.base.reflect.annocation.ioc.annocation;

import java.lang.annotation.*;

/**
 * 组件
 *
 * @author Michael
 * @create 2020-08-05 17:04
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Component {

    String value() default "";

}
