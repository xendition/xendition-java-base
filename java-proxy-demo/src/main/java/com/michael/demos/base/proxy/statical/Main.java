package com.michael.demos.base.proxy.statical;

/**
 * 类功能描述:
 * <pre>
 *   静态代理 - 启动类(client)
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 10:13
 */
public class Main {

    public static void main(String[] args) {

        IService service = new RealService();
        ServiceProxy serviceProxy = new ServiceProxy(service);
        for (int i = 0; i < 10; i++) {
            System.out.println("第 " + i + " 次执行");
            serviceProxy.doSomething();
            System.out.println();
        }
    }
}
