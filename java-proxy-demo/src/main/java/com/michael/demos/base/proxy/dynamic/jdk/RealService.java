package com.michael.demos.base.proxy.dynamic.jdk;

import java.util.Random;

/**
 * 类功能描述:
 * <pre>
 *   JDK动态代理 - 被代理对象
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 10:02
 */
public class RealService implements IService {

    @Override
    public void doSomething() {
        System.out.println("RealService:doSomething ……");
        Random random = new Random();
        int i = random.nextInt(10);
        if (i % 2 == 0) {
            throw new RuntimeException("RealService:发生异常");
        }
    }
}

