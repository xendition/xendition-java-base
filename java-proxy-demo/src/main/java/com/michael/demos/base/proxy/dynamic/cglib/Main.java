package com.michael.demos.base.proxy.dynamic.cglib;

import net.sf.cglib.proxy.Enhancer;

/**
 * 类功能描述:
 * <pre>
 *   CGLIB动态代理 - 启动类(client)
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 17:39
 */
public class Main {

    public static void main(String[] args) {

        // 生成的代理类位置
        System.getProperties().put("cglib.debugLocation", "com");

        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(RealService.class);
        // 设置enhancer的回调对象
        enhancer.setCallback(new ServiceIntercepter());
        // 创建代理对象
        RealService proxy = (RealService) enhancer.create();
        // 通过代理对象调用目标方法
        proxy.doSomething();
        proxy.finalTest("test");
    }
}
