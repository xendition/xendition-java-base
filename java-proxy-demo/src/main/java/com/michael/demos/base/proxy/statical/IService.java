package com.michael.demos.base.proxy.statical;

/**
 * 类功能描述:
 * <pre>
 *   静态代理 - 接口
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 9:58
 */
public interface IService {
    void doSomething();
}
