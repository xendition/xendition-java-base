package com.michael.demos.base.proxy.dynamic.jdk;

import java.lang.reflect.Proxy;

/**
 * 类功能描述:
 * <pre>
 *   JDK动态代理 - 启动类(client)
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 10:13
 */
public class Main {

    /**
     * JDK动态代理过程分析：
     * 1. 运行时为接口创建代理类的字节码文件
     * 2. 使用类加载器将.class字节码加载到内存
     * 3. 创建代理类的实例对象，执行被代理类的目标方法
     */
    public static void main(String[] args) {

        // 将 Proxy.newProxyInstance 生成的动态代理类存放到磁盘中
        // 默认生成路径 com.sun.proxy 包下
        System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");

        //IService service = new RealService();
        ServiceHandler handler = new ServiceHandler();

        IService proxyInstance = (IService) Proxy.newProxyInstance(
                handler.getClass().getClassLoader(),
                // 三种方式都可以
                new Class[]{IService.class},
                //service.getClass().getInterfaces(),
                //RealService.class.getInterfaces(),
                handler
        );

        for (int i = 0; i < 10; i++) {
            System.out.println("第 " + i + " 次执行");
            proxyInstance.doSomething();
            System.out.println();
        }
    }
}
