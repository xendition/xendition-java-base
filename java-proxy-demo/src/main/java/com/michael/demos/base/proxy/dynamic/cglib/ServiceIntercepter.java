package com.michael.demos.base.proxy.dynamic.cglib;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * 类功能描述:
 * <pre>
 *   CGLIB动态代理 - 增强处理类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 17:34
 */
public class ServiceIntercepter implements MethodInterceptor {

    @Override
    public Object intercept(
            Object o, Method method, Object[] objects, MethodProxy methodProxy
    ) throws Throwable {
        around();
        before();

        Exception ex = null;
        Object result = null;
        try {
            result = methodProxy.invokeSuper(o, objects);
        } catch (Exception e) {
            ex = e;
        }
        around();
        if (ex == null) {
            after();
            afterReturning();
        } else {
            after();
            afterThrowing(ex);
        }
        return result;
    }


    /** 代理增强 - 代理方法执行前后 */
    private void around() {
        System.out.println("[Proxy]:around ……");
    }

    /** 代理增强 - 代理方法执行前 */
    private void before() {
        System.out.println("[Proxy]:before ……");
    }

    /** 代理增强 - 代理方法执行后 */
    private void after() {
        System.out.println("[Proxy]:after ……");
    }

    /** 代理增强 - 代理方法执行异常后 */
    private void afterThrowing(Exception e) {
        //e.printStackTrace();
        System.out.println("[Proxy]: ！！！afterThrowing ……");
    }

    /** 代理增强 - 代理方法正常执行完成后 */
    private void afterReturning() {
        System.out.println("[Proxy]:afterReturning ……");
    }
}
