package com.michael.demos.base.proxy.dynamic.cglib;


import java.util.Random;

/**
 * 类功能描述:
 * <pre>
 *   CGLIB动态代理 - 被代理对象
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/4 10:02
 */
public class RealService {

    /** 测试 final 修饰的方法 */
    public final void finalTest(String value) {
        System.out.println("RealService:finalTest ……" + value);
    }

    public void doSomething() {
        System.out.println("RealService:doSomething ……");
        Random random = new Random();
        int i = random.nextInt(10);
        if (i % 2 == 0) {
            throw new RuntimeException("RealService:发生异常");
        }
    }
}

