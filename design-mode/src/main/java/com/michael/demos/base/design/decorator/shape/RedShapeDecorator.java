package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   扩展了 ShapeDecorator 类的实体装饰类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:12
 */
public class RedShapeDecorator extends ShapeDecorator {

    public RedShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        // 方法增强
        setRedBorder(decoratedShape);
    }

    private void setRedBorder(Shape decoratedShape) {
        System.out.println("加上了红色边框");
    }
}
