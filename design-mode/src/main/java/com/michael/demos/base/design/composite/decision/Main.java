package com.michael.demos.base.design.composite.decision;

import com.michael.demos.base.design.composite.decision.model.TreeRich;
import com.michael.demos.base.design.composite.decision.model.vo.EngineResultVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeLinkVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeRootVO;
import com.michael.demos.base.design.composite.decision.service.engine.IEngine;
import com.michael.demos.base.design.composite.decision.service.engine.impl.TreeEngineHandle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类功能描述:
 * <pre>
 *   决策测试
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 15:39
 */
public class Main {


    private static TreeRich treeRich;

    static {

        // 节点：1
        TreeNodeVO treeNode_01 = new TreeNodeVO();

        treeNode_01.setTreeId(10001L);
        treeNode_01.setTreeNodeId(1L);
        treeNode_01.setNodeType(1);
        treeNode_01.setNodeValue(null);
        treeNode_01.setRuleKey("userGender");
        treeNode_01.setRuleDesc("用户性别[男/女]");

        // 链接：1->11
        TreeNodeLinkVO treeNodeLink_11 = new TreeNodeLinkVO();
        treeNodeLink_11.setNodeIdFrom(1L);
        treeNodeLink_11.setNodeIdTo(11L);
        treeNodeLink_11.setRuleLimitType(1);
        treeNodeLink_11.setRuleLimitValue("man");

        // 链接：1->12
        TreeNodeLinkVO treeNodeLink_12 = new TreeNodeLinkVO();
        treeNodeLink_12.setNodeIdTo(1L);
        treeNodeLink_12.setNodeIdTo(12L);
        treeNodeLink_12.setRuleLimitType(1);
        treeNodeLink_12.setRuleLimitValue("woman");

        List<TreeNodeLinkVO> treeNodeLinkList_1 = new ArrayList<>();
        treeNodeLinkList_1.add(treeNodeLink_11);
        treeNodeLinkList_1.add(treeNodeLink_12);

        treeNode_01.setTreeNodeLinkVOList(treeNodeLinkList_1);

        // 节点：11
        TreeNodeVO treeNode_11 = new TreeNodeVO();
        treeNode_11.setTreeId(10001L);
        treeNode_11.setTreeNodeId(11L);
        treeNode_11.setNodeType(1);
        treeNode_11.setNodeValue(null);
        treeNode_11.setRuleKey("userAge");
        treeNode_11.setRuleDesc("用户年龄");

        // 链接：11->111
        TreeNodeLinkVO treeNodeLink_111 = new TreeNodeLinkVO();
        treeNodeLink_111.setNodeIdFrom(11L);
        treeNodeLink_111.setNodeIdTo(111L);
        treeNodeLink_111.setRuleLimitType(3);
        treeNodeLink_111.setRuleLimitValue("25");

        // 链接：11->112
        TreeNodeLinkVO treeNodeLink_112 = new TreeNodeLinkVO();
        treeNodeLink_112.setNodeIdFrom(11L);
        treeNodeLink_112.setNodeIdTo(112L);
        treeNodeLink_112.setRuleLimitType(5);
        treeNodeLink_112.setRuleLimitValue("25");

        List<TreeNodeLinkVO> treeNodeLinkList_11 = new ArrayList<>();
        treeNodeLinkList_11.add(treeNodeLink_111);
        treeNodeLinkList_11.add(treeNodeLink_112);

        treeNode_11.setTreeNodeLinkVOList(treeNodeLinkList_11);

        // 节点：12
        TreeNodeVO treeNode_12 = new TreeNodeVO();
        treeNode_12.setTreeId(10001L);
        treeNode_12.setTreeNodeId(12L);
        treeNode_12.setNodeType(1);
        treeNode_12.setNodeValue(null);
        treeNode_12.setRuleKey("userAge");
        treeNode_12.setRuleDesc("用户年龄");

        // 链接：12->121
        TreeNodeLinkVO treeNodeLink_121 = new TreeNodeLinkVO();
        treeNodeLink_121.setNodeIdFrom(12L);
        treeNodeLink_121.setNodeIdTo(121L);
        treeNodeLink_121.setRuleLimitType(3);
        treeNodeLink_121.setRuleLimitValue("25");

        // 链接：12->122
        TreeNodeLinkVO treeNodeLink_122 = new TreeNodeLinkVO();
        treeNodeLink_122.setNodeIdFrom(12L);
        treeNodeLink_122.setNodeIdTo(122L);
        treeNodeLink_122.setRuleLimitType(5);
        treeNodeLink_122.setRuleLimitValue("25");

        List<TreeNodeLinkVO> treeNodeLinkList_12 = new ArrayList<>();
        treeNodeLinkList_12.add(treeNodeLink_121);
        treeNodeLinkList_12.add(treeNodeLink_122);

        treeNode_12.setTreeNodeLinkVOList(treeNodeLinkList_12);

        // 节点：111
        TreeNodeVO treeNode_111 = new TreeNodeVO();
        treeNode_111.setTreeId(10001L);
        treeNode_111.setTreeNodeId(111L);
        treeNode_111.setNodeType(2);
        treeNode_111.setNodeValue("结果A");

        // 节点：112
        TreeNodeVO treeNode_112 = new TreeNodeVO();
        treeNode_112.setTreeId(10001L);
        treeNode_112.setTreeNodeId(112L);
        treeNode_112.setNodeType(2);
        treeNode_112.setNodeValue("结果B");

        // 节点：121
        TreeNodeVO treeNode_121 = new TreeNodeVO();
        treeNode_121.setTreeId(10001L);
        treeNode_121.setTreeNodeId(121L);
        treeNode_121.setNodeType(2);
        treeNode_121.setNodeValue("结果C");

        // 节点：122
        TreeNodeVO treeNode_122 = new TreeNodeVO();
        treeNode_122.setTreeId(10001L);
        treeNode_122.setTreeNodeId(122L);
        treeNode_122.setNodeType(2);
        treeNode_122.setNodeValue("结果D");

        // 树根
        TreeRootVO treeRoot = new TreeRootVO();
        treeRoot.setTreeId(10001L);
        treeRoot.setTreeRootNodeId(1L);
        treeRoot.setTreeName("规则决策树");

        Map<Long, TreeNodeVO> treeNodeMap = new HashMap<>();
        treeNodeMap.put(1L, treeNode_01);
        treeNodeMap.put(11L, treeNode_11);
        treeNodeMap.put(12L, treeNode_12);
        treeNodeMap.put(111L, treeNode_111);
        treeNodeMap.put(112L, treeNode_112);
        treeNodeMap.put(121L, treeNode_121);
        treeNodeMap.put(122L, treeNode_122);

        treeRich = new TreeRich(treeRoot, treeNodeMap);
    }

    public static void main(String[] args) {

        System.out.println(treeRich);

        IEngine treeEngineHandle = new TreeEngineHandle();

        Map<String, String> decisionMatter = new HashMap<>();
        decisionMatter.put("gender", "man");
        decisionMatter.put("age", "29");

        EngineResultVO result = treeEngineHandle.process(10001L, "Oli09pLkdjh", treeRich, decisionMatter);
        System.out.println(result);

    }


}
