package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   圆
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:10
 */
public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("画了一个圆");
    }
}
