package com.michael.demos.base.design.state;

import com.michael.demos.base.design.state.leave.Context;
import com.michael.demos.base.design.state.leave.DirectorState;

/**
 * 类功能描述:
 * <pre>
 *   状态模式 - 测试类 - 请假
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 18:00
 */
public class LeaveMain {
    public static void main(String[] args) {
        Context context = new Context();
        context.setState(new DirectorState());
        // 需要自己去找对应的人审批
        context.request();
        context.request();
        context.request();
    }
}
