package com.michael.demos.base.design.state.activity.entity;

import com.michael.demos.base.design.state.activity.enums.StatusEnum;

import java.util.Date;

/**
 * 类功能描述:
 * <pre>
 *   活动
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 10:40
 */
public class ActivityInfo {

	/** 活动ID */
	private Integer activityId;
	/** 活动名称 */
	private String activityName;
	/** 活动状态 */
	private StatusEnum statusEnum;
	/** 开始时间 */
	private Date beginTime;
	/** 结束时间 */
	private Date endTime;

	public ActivityInfo() {}

	public ActivityInfo(Integer activityId, String activityName, StatusEnum statusEnum, Date beginTime, Date endTime) {
		this.activityId = activityId;
		this.activityName = activityName;
		this.statusEnum = statusEnum;
		this.beginTime = beginTime;
		this.endTime = endTime;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public StatusEnum getStatusEnum() {
		return statusEnum;
	}

	public void setStatusEnum(StatusEnum statusEnum) {
		this.statusEnum = statusEnum;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "ActivityInfo{" +
				"activityId=" + activityId +
				", activityName='" + activityName + '\'' +
				", statusEnum=" + statusEnum +
				", beginTime=" + beginTime +
				", endTime=" + endTime +
				'}';
	}
}
