package com.michael.demos.base.design.mediator.database;

/**
 * 类功能描述:
 * <pre>
 *   数据库类型枚举
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:46
 */
public enum DatabaseEnum {
	Mysql, Redis, Elasticsearch
}
