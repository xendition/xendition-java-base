package com.michael.demos.base.design.composite.decision.service.engine;


import com.michael.demos.base.design.composite.decision.model.TreeRich;
import com.michael.demos.base.design.composite.decision.model.vo.EngineResultVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeRootVO;
import com.michael.demos.base.design.composite.decision.service.logic.LogicFilter;

import java.util.Map;

/**
 *
 */
public abstract class EngineBase extends EngineConfig implements IEngine {

    @Override
    public abstract EngineResultVO process(
            Long treeId, String userId, TreeRich treeRich, Map<String, String> decisionMatter
    );

    protected TreeNodeVO engineDecisionMaker(
            TreeRich treeRich, Long treeId, String userId, Map<String, String> decisionMatter
    ) {
        TreeRootVO treeRoot = treeRich.getTreeRoot();
        Map<Long, TreeNodeVO> treeNodeMap = treeRich.getTreeNodeMap();
        // 规则树根ID
        Long rootNodeId = treeRoot.getTreeRootNodeId();
        TreeNodeVO treeNodeInfo = treeNodeMap.get(rootNodeId);
        //节点类型[NodeType]；1子叶、2果实
        while (treeNodeInfo.getNodeType().equals(1)) {
            String ruleKey = treeNodeInfo.getRuleKey();
            LogicFilter logicFilter = logicFilterMap.get(ruleKey);
            String matterValue = logicFilter.matterValue(treeId, userId, decisionMatter);
            Long nextNode = logicFilter.filter(matterValue, treeNodeInfo.getTreeNodeLinkVOList());
            treeNodeInfo = treeNodeMap.get(nextNode);
            //logger.info("决策树引擎=>{} userId：{} treeId：{} treeNode：{} ruleKey：{} matterValue：{}", treeRoot.getTreeName
            // (), userId, treeId, treeNodeInfo.getTreeNodeId(), ruleKey, matterValue);
        }
        return treeNodeInfo;
    }

}
