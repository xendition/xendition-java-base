package com.michael.demos.base.design.observer.jdk;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 类功能描述:
 * <pre>
 *   报社
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:37
 */
public class NewsOffice extends Observable {

    private static final long DELAY = 2 * 1000;

    public NewsOffice() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            private int titleCount = 1;
            private int contentCount = 1;

            @Override
            public void run() {
                setChanged(); //调用setChagned方法，将changed域设置为true，这样才能通知到观察者们
                notifyObservers(new News("title:" + titleCount++, "content:" + contentCount++));
            }
        }, DELAY, 1000);
    }


}
