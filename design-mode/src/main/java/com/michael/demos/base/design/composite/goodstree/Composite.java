package com.michael.demos.base.design.composite.goodstree;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   安全组合模式 —— 容器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 17:06
 */
public class Composite extends Component {

    /**
     * 用来存储组合对象中包含的子组件对象
     */
    private List<Component> childComponents = new ArrayList<>();
    /**
     * 组合对象的名字
     */
    private String name;

    public Composite(String name) {
        this.name = name;
    }

    /**
     * 向组合对象加入被它包含的其它组合对象
     *
     * @param c 被它包含的其它组合对象
     */
    public void addChild(Component c) {
        this.childComponents.add(c);
    }

    /**
     * 输出组合对象自身的结构
     *
     * @param preStr 前缀，主要是按照层级拼接的空格，实现向后缩进
     */
    @Override
    public void printStruct(String preStr) {
        //先把自己输出去
        System.out.println(preStr + "+" + this.name);
        //然后添加俩空格，表示向后缩进俩空格，输出自己包含的叶子对象
        preStr += "  ";

        //输出当前对象的子组合对象
        for (Component c : childComponents) {
            //递归输出每个子组合对象
            c.printStruct(preStr);
        }
    }
}
