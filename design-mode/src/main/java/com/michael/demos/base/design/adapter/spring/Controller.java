package com.michael.demos.base.design.adapter.spring;

/**
 * 类功能描述:
 * <pre>
 *   多种Controller实现
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:47
 */
public interface Controller {}

class HttpController implements Controller {
	public void doHttpHandler() {
		System.out.println("http....");
	}
}

class SimpleController implements Controller {
	public void doSimplerHandler() {
		System.out.println("simple...");
	}
}

class AnnotationController implements Controller {
	public void doAnnotationHandler() {
		System.out.println("annotation...");
	}
}
