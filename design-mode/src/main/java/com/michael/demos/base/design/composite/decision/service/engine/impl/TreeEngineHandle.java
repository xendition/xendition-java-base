package com.michael.demos.base.design.composite.decision.service.engine.impl;

import com.michael.demos.base.design.composite.decision.model.TreeRich;
import com.michael.demos.base.design.composite.decision.model.vo.EngineResultVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeVO;
import com.michael.demos.base.design.composite.decision.service.engine.EngineBase;

import java.util.Map;

public class TreeEngineHandle extends EngineBase {

    @Override
    public EngineResultVO process(Long treeId, String userId, TreeRich treeRich, Map<String, String> decisionMatter) {
        // 决策流程
        TreeNodeVO treeNode = engineDecisionMaker(treeRich, treeId, userId, decisionMatter);
        // 决策结果
        return new EngineResultVO(userId, treeId, treeNode.getTreeNodeId(), treeNode.getNodeValue());
    }

}
