package com.michael.demos.base.design.adapter;

/**
 * 类功能描述:
 * <pre>
 *   适配器示例 —— 类适配器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:39
 */
public class AdapterDemo2 {
	public static void main(String[] args) {
		System.out.println(" === 类适配器模式 ===");
		Phone phone = new Phone();
		phone.charging(new VoltageAdapter2());
	}
}


class VoltageAdapter2 extends Voltage220V implements IVoltage5V {

	@Override
	public int output5V() {
		// 获取到220V电压
		int srcV = output220V();
		// 转成5v
		int dstV = srcV / 44;
		return dstV;
	}

}