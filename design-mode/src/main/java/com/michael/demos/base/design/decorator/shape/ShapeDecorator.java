package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   实现了 Shape 接口的抽象装饰类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:11
 */
public abstract class ShapeDecorator implements Shape {

    protected Shape decoratedShape;

    public ShapeDecorator(Shape decoratedShape) {
        this.decoratedShape = decoratedShape;
    }

    @Override
    public void draw() {
        decoratedShape.draw();
    }
}
