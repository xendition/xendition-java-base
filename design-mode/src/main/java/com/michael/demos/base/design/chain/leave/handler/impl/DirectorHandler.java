package com.michael.demos.base.design.chain.leave.handler.impl;

import com.michael.demos.base.design.chain.leave.LeaveRequest;
import com.michael.demos.base.design.chain.leave.handler.AbstractHandler;

/**
 * 类功能描述:
 * <pre>
 *   责任链模式 - 具体处理类 - 主管审批
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 16:49
 */
public class DirectorHandler extends AbstractHandler {

    public DirectorHandler() {
        super("王五");
    }

    @Override
    public boolean process(LeaveRequest leaveRequest) {

        int nextInt = RANDOM.nextInt(100);
        // 是否同意
        boolean result = (nextInt % 2) == 0;

        System.out.println("主管<" + name + "> 审批结果 ：" + (result ? "同意" : "不同意"));
        Integer days = leaveRequest.getDays();
        if (!result) {
            return false;
        } else if (days <= LEVEL1) {
            return result;
        }
        return next.process(leaveRequest);
    }
}
