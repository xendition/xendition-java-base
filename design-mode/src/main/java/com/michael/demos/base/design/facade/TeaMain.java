package com.michael.demos.base.design.facade;

import com.michael.demos.base.design.facade.tea.TeaHouseFacade;
import com.michael.demos.base.design.facade.tea.entity.Man;
import com.michael.demos.base.design.facade.tea.entity.TeaLeaf;
import com.michael.demos.base.design.facade.tea.entity.Teawater;
import com.michael.demos.base.design.facade.tea.entity.Water;
import com.michael.demos.base.design.facade.tea.service.KettleService;
import com.michael.demos.base.design.facade.tea.service.TeasetService;
import com.michael.demos.base.design.facade.tea.type.TeaTypeEnum;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 测试类 - 喝茶
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 11:09
 */
public class TeaMain {

    public static void main(String[] args) {

        noFacade();
        facade();
    }

    public static void facade() {

        System.out.println("\n使用门面模式 ===================  \n");

        TeaHouseFacade teaHouseFacade = new TeaHouseFacade("老舍茶馆");

        Man zhangsan = new Man("张三");
        Teawater teawater = teaHouseFacade.makeTea(TeaTypeEnum.BLC);
        zhangsan.drinkTea(teawater);
        System.out.println();

        Man lisi = new Man("李四");
        Teawater teawater1 = teaHouseFacade.makeTea(TeaTypeEnum.LJ);
        lisi.drinkTea(teawater1);

        System.out.println("\n使用门面模式 =================== END \n");
    }

    /** 不使用门面模式的喝茶方式 */
    public static void noFacade() {

        System.out.println("\n不使用门面模式 ===================  \n");
        KettleService kettleService = new KettleService();
        TeasetService teasetService = new TeasetService();

        TeaLeaf teaLeaf1 = new TeaLeaf("西湖龙井");
        TeaLeaf teaLeaf2 = new TeaLeaf("碧螺春");

        Man zhangsan = new Man("张三");
        Water water1 = new Water();
        kettleService.waterBurning(zhangsan, water1, 4);
        Teawater teawater1 = teasetService.makeTeaWater(zhangsan, water1, teaLeaf1);
        zhangsan.drinkTea(teawater1);

        System.out.println();

        Man lisi = new Man("李四");
        Water water2 = new Water(10, 150);
        kettleService.waterBurning(lisi, water2, 4);
        Teawater teawater2 = teasetService.makeTeaWater(lisi, water2, teaLeaf2);
        lisi.drinkTea(teawater2);


        System.out.println("\n不使用门面模式 =================== END \n");
    }
}
