package com.michael.demos.base.design.strategy.coupon;

import com.michael.demos.base.design.strategy.coupon.impl.ZjCouponStrategy;

import java.math.BigDecimal;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 17:26
 */
public class Client {

    public static void main(String[] args) {

        CouponService couponService = new CouponService(new ZjCouponStrategy());
        BigDecimal finalPrice = couponService.discountAmount(BigDecimal.valueOf(100L), BigDecimal.valueOf(10L));

        System.out.print("使用直减优惠券后的价格\t");
        System.out.println(finalPrice);
    }
}
