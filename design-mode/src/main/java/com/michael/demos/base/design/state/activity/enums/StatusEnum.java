package com.michael.demos.base.design.state.activity.enums;

/**
 * 类功能描述:
 * <pre>
 *   活动状态
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 10:41
 */
public enum StatusEnum {
	// 1创建编辑、2待审核、3审核通过(任务扫描成活动中)、4审核拒绝(可以撤审到编辑状态)、5活动中、6活动关闭、7活动开启(任务扫描成活动中)

	/** 创建/编辑 */
	Editing,
	/** 待审核 */
	Check,
	/** 审核通过 */
	Pass,
	/** 审核拒绝 */
	Refuse,
	/** 活动中 */
	Doing,
	/** 活动关闭 */
	Close,
	/** 活动开启 */
	Open,
	;
}
