package com.michael.demos.base.design.bridge.phone;

/**
 * 类功能描述:
 * <pre>
 *   桥接模式 - 抽象类 - 手机
 *
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/10 16:26
 */
public abstract class Phone {

    /** 定义一个实现类接口 */
    private final Brand brand;

    public Phone(Brand brand) {
        this.brand = brand;
    }

    public void open() {
        this.brand.open();
    }

    public void close() {
        this.brand.close();
    }

    public void call() {
        this.brand.call();
    }
}
