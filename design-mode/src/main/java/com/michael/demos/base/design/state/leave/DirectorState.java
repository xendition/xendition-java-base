package com.michael.demos.base.design.state.leave;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 17:58
 */
public class DirectorState extends State {
	@Override
	void handle() {
		System.out.println("主管审批通过，下一个经理审批");
		context.setState(new ManagerState());
	}
}
