package com.michael.demos.base.design.bridge.phone;

/**
 * 类功能描述:
 * <pre>
 *   桥接模式 - 实现类接口 - 手机品牌
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/10 16:19
 */
public interface Brand {

    void open();

    void close();

    void call();
}
