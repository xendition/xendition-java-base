package com.michael.demos.base.design.observer.newspaper;


/**
 * 类功能描述:
 * <pre>
 *   观察者接口定义 —— 消费者
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:29
 */
public interface Consumer {
    void receive(News model);
}
