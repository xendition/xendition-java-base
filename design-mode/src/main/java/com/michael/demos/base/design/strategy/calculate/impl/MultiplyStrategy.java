package com.michael.demos.base.design.strategy.calculate.impl;

import com.michael.demos.base.design.strategy.calculate.ICalculateStrategy;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 策略实现——乘法
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/31 9:19
 */
public class MultiplyStrategy implements ICalculateStrategy {
    @Override
    public Number doCalculate(int first, int second) {
        return first * second;
    }
}
