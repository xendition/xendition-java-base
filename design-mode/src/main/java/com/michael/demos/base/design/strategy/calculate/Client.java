package com.michael.demos.base.design.strategy.calculate;

/**
 * @author Michael
 * @version 1.0
 * @date 2020/12/31 9:23
 */
public class Client {

    public static void main(String[] args) {
        System.out.println(CalculateUtil.add(1, 2));
        System.out.println(CalculateUtil.multiply(20, 2));
    }
}
