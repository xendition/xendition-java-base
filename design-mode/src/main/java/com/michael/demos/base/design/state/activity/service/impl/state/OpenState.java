package com.michael.demos.base.design.state.activity.service.impl.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.Result;

/**
 * 活动状态；活动开启
 */
public class OpenState implements State {

//    @Override
//    public Result arraignment(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动开启不可提审");
//    }
//
//    @Override
//    public Result checkPass(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动开启不可审核通过");
//    }
//
//    @Override
//    public Result checkRefuse(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动开启不可审核拒绝");
//    }
//
//    @Override
//    public Result checkRevoke(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动开启不可撤销审核");
//    }

	@Override
	public Result close(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Close);
		return new Result(SUCCESS_CODE, "活动关闭完成");
	}

//    @Override
//    public Result open(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动不可重复开启");
//    }

	@Override
	public Result doing(ActivityInfo info) {
		return new Result(SUCCESS_CODE, "活动变更活动中完成");
	}

}