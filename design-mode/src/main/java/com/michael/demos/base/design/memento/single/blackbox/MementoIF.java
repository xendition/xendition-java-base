package com.michael.demos.base.design.memento.single.blackbox;

/**
 * 类功能描述:
 * <pre>
 *  黑箱模式实现备忘录 —— 接口标识(不需要定义任何方法)
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:23
 */
public interface MementoIF {
}
