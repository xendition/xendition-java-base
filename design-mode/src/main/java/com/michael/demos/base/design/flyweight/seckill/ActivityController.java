package com.michael.demos.base.design.flyweight.seckill;

/**
 * 类功能描述:
 * <pre>
 *   调用类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 15:46
 */
public class ActivityController {

    private RedisUtil redisUtil = new RedisUtil();

    public Activity queryActivityInfo(Long id) {
        Activity activity = ActivityFactory.getActivity(id);
        // 模拟从Redis中获取库存变化信息
        Stock stock = new Stock(1000, redisUtil.getStockUsed());
        activity.setStock(stock);
        return activity;
    }

}
