package com.michael.demos.base.design.facade.tea.service;

import com.michael.demos.base.design.facade.tea.entity.Man;
import com.michael.demos.base.design.facade.tea.entity.Water;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 子系统 - 热水壶
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 10:41
 */
public class KettleService {
    public void waterBurning(Man who, Water water, int burnTime) {
        // 烧水，计算最终温度
        int finalTemperate = Math.min(100, water.getTemperature() + burnTime * 20);
        water.setTemperature(finalTemperate);
        System.out.println(who.getName() + "使用水壶烧水，最终水温" + finalTemperate + "℃");
    }
}
