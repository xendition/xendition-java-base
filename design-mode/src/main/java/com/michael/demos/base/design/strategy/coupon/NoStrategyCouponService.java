package com.michael.demos.base.design.strategy.coupon;

import java.math.BigDecimal;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 优惠券服务类 —— 不使用策略模式
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 14:16
 */
public class NoStrategyCouponService {

    /**
     * 获取订单最终的价格
     * 这种方式不可取，仅做为反例。
     * 实际上的优惠金额计算比这个复杂的多。
     */
    public BigDecimal discountAmount(int couponType, BigDecimal orderPrice, BigDecimal couponPrice) {

        switch (couponType) {
            case 1: {
                // 直减券
                if (orderPrice.compareTo(couponPrice) > 0) {
                    return orderPrice.subtract(couponPrice);
                }
                return BigDecimal.ZERO;
            }
            case 2: {
                // 满减券
                // 假如是订单满20减优惠券金额的额度
                BigDecimal conditionPrice = BigDecimal.valueOf(20L);
                if (orderPrice.compareTo(conditionPrice) > 0) {
                    // 直减券
                    if (orderPrice.compareTo(couponPrice) > 0) {
                        return orderPrice.subtract(couponPrice);
                    }
                    return BigDecimal.ZERO;
                }
                return orderPrice;
            }
            case 3: {
                // 折扣券
                // 假如是订单满20可使用对应折扣的优惠券
                BigDecimal conditionPrice = BigDecimal.valueOf(20L);
                if (orderPrice.compareTo(conditionPrice) > 0) {
                    // 这里的优惠额度是折扣度
                    return orderPrice.multiply(couponPrice);
                }
                return orderPrice;
            }
            case 4: {
                // 秒杀券
                // 固定支付一个金额
                return couponPrice;
            }
            default: {
                // 默认无优惠券
                return orderPrice;
            }
        }
    }
}
