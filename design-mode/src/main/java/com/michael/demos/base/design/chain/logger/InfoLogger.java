package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:44
 */
public final class InfoLogger extends AbstractLogger {

	InfoLogger() {
		super(LogLevel.INFO);
	}

	@Override
	protected void write(String message) {
		System.out.println("\u001B[33m[INFO] " + message);
	}
}
