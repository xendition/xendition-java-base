package com.michael.demos.base.design.flyweight;

import com.michael.demos.base.design.flyweight.seckill.Activity;
import com.michael.demos.base.design.flyweight.seckill.ActivityController;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 15:47
 */
public class SeckillMain {

    private static final ActivityController C = new ActivityController();

    public static void main(String[] args) throws InterruptedException {
        for (int idx = 0; idx < 100; idx++) {
            Long req = 10001L;
            Activity activity = C.queryActivityInfo(req);
            System.out.println("测试结果");
            System.out.println("ID" + req);
            System.out.println(activity);
            Thread.sleep(100);
        }
    }
}
