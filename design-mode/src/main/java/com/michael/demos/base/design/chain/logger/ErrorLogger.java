package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:47
 */
public final class ErrorLogger extends AbstractLogger {

	ErrorLogger() {
		super(LogLevel.ERROR);
		System.out.println("ErrorLogger initing ……");
		InfoLogger info = new InfoLogger();
		info.setNextLogger(new DebugLogger());
		setNextLogger(info);
	}

	@Override
	protected void write(String message) {
		System.out.println("\u001B[31m[ERROR] " + message);
	}
}
