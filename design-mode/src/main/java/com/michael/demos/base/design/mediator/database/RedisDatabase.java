package com.michael.demos.base.design.mediator.database;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   Redis
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:41
 */
public class RedisDatabase extends AbstractDatabase {

	private List<String> dataset = new ArrayList<String>();

	public RedisDatabase(AbstractMediator mediator) {
		super(mediator);
	}

	@Override
	public void addData(String data) {
		System.out.println("Redis 添加数据：" + data);
		this.dataset.add(data);
	}

	@Override
	public void add(String data) {
		addData(data);
		// 数据同步作业交给中介者管理
		this.mediator.sync(DatabaseEnum.Redis, data);
	}

	public void cache() {
		System.out.println("Redis 缓存的数据：" + this.dataset.toString());
	}
}
