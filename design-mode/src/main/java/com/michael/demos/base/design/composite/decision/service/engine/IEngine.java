package com.michael.demos.base.design.composite.decision.service.engine;


import com.michael.demos.base.design.composite.decision.model.TreeRich;
import com.michael.demos.base.design.composite.decision.model.vo.EngineResultVO;

import java.util.Map;

public interface IEngine {

    EngineResultVO process(
            final Long treeId, final String userId, TreeRich treeRich, final Map<String, String> decisionMatter
    );
}
