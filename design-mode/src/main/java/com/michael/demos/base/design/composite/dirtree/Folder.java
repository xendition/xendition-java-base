package com.michael.demos.base.design.composite.dirtree;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   透明组合模式 —— 文件夹
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 16:40
 */
public class Folder extends Component {

    /** 文件夹名称 */
    private String name;
    /** 文件夹级别 */
    public Integer level;
    /** 文件夹下级构建 */
    private List<Component> children = new ArrayList<Component>();

    public Folder(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void add(Component component) {
        this.children.add(component);
    }

    @Override
    public void remove(Component component) {
        this.children.remove(component);
    }

    @Override
    public void print() {

        System.out.println(this.getName());
        if (this.level == null) {
            this.level = 1;
        }
        String prefix = "";
        for (int i = 0; i < this.level; i++) {
            prefix += "\t|- ";
        }
        for (Component component : this.children) {
            if (component instanceof Folder) {
                ((Folder) component).level = this.level + 1;
            }
            System.out.print(prefix);
            component.print();
        }
        this.level = null;
    }
}
