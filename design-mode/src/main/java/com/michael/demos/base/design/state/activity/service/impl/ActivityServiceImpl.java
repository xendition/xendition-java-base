package com.michael.demos.base.design.state.activity.service.impl;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.ActivityService;
import com.michael.demos.base.design.state.activity.service.Result;
import com.michael.demos.base.design.state.activity.service.impl.state.*;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ActivityServiceImpl implements ActivityService {

	private final Map<Integer, ActivityInfo> DB = new ConcurrentHashMap<Integer, ActivityInfo>();

	/** 保存所有状态 */
	private final Map<StatusEnum, State> stateMap = new ConcurrentHashMap<StatusEnum, State>();

	public ActivityServiceImpl() {
		// 待审核
		stateMap.put(StatusEnum.Check, new CheckState());
		// 已关闭
		stateMap.put(StatusEnum.Close, new CloseState());
		// 活动中
		stateMap.put(StatusEnum.Doing, new DoingState());
		// 编辑中
		stateMap.put(StatusEnum.Editing, new EditingState());
		// 已开启
		stateMap.put(StatusEnum.Open, new OpenState());
		// 审核通过
		stateMap.put(StatusEnum.Pass, new PassState());
		// 审核拒绝
		stateMap.put(StatusEnum.Refuse, new RefuseState());
	}

	@Override
	public Result arraignment(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).arraignment(info);
	}

	@Override
	public Result checkPass(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).checkPass(info);
	}

	@Override
	public Result checkRefuse(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).checkRefuse(info);
	}

	@Override
	public Result checkRevoke(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).checkRevoke(info);
	}

	@Override
	public Result close(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).close(info);
	}

	@Override
	public Result open(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).open(info);
	}

	@Override
	public Result doing(ActivityInfo info) {
		return stateMap.get(info.getStatusEnum()).doing(info);
	}

	@Override
	public void add(ActivityInfo info) {
		DB.put(info.getActivityId(), info);
	}

	@Override
	public ActivityInfo queryById(Integer activityId) {
		return DB.get(activityId);
	}

	@Override
	public ActivityInfo updateById(ActivityInfo info) {
		DB.put(info.getActivityId(), info);
		return info;
	}

	@Override
	public void removeById(Integer activityId) {
		DB.remove(activityId);
	}
}
