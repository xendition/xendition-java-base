package com.michael.demos.base.design.bridge;

import com.michael.demos.base.design.bridge.phone.Camera2;
import com.michael.demos.base.design.bridge.phone.Camera3;
import com.michael.demos.base.design.bridge.phone.Phone;
import com.michael.demos.base.design.bridge.phone.impl.Apple;
import com.michael.demos.base.design.bridge.phone.impl.Huawei;
import com.michael.demos.base.design.bridge.phone.impl.Vivo;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/10 16:31
 */
public class PhoneMain {

    public static void main(String[] args) {

        // 苹果双摄
        Phone p1 = new Camera2(new Apple());
        p1.open();
        p1.call();
        p1.close();

        System.out.println();

        // 华为三摄
        Phone p2 = new Camera3(new Huawei());
        p2.open();
        p2.call();
        p2.close();

        System.out.println();

        // vivo 三摄
        Phone p3 = new Camera3(new Vivo());
        p3.open();
        p3.call();
        p3.close();
    }
}
