package com.michael.demos.base.design.observer;


import com.michael.demos.base.design.observer.jdk.NewsOffice;
import com.michael.demos.base.design.observer.jdk.User;

import java.util.Observable;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:27
 */
public class JdkMain {

    public static void main(String[] args) {
        Observable provider = new NewsOffice();
        User user;
        for (int i = 0; i < 10; i++) {
            user = new User("user:" + i);
            provider.addObserver(user);
        }
    }
}
