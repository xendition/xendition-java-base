package com.michael.demos.base.design.memento.single.whitebox;

/**
 * 类功能描述:
 * <pre>
 *  白箱模式实现备忘录 —— 客户端角色类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:19
 */
public class Client {

	/**
	 * 1. 将发起人对象的状态设置为“On”。
	 * 2. 调用发起人角色的createMemento()方法，创建一个备忘录的对象将这个状态存储起来。
	 * 3. 将备忘录对象存储到负责人对象中去。
	 */
	public static void main(String[] args) {

		Originator originator = new Originator();
		Caretaker caretaker = new Caretaker();
		//改变发起人对象的状态
		originator.setState("On");
		//创建备忘录对象，并将发起人对象的状态存储起来
		caretaker.saveMemento(originator.createMemento());
		//修改发起人对象的状态
		originator.setState("Off");
		//恢复发起人对象的状态
		originator.restoreMemento(caretaker.retrieveMenento());

		//发起人对象的状态
		System.out.println("发起人对象的当前状态为：" + originator.getState());
	}
}
