package com.michael.demos.base.design.facade.tea;

import com.michael.demos.base.design.facade.tea.entity.Teawater;
import com.michael.demos.base.design.facade.tea.type.TeaMaker;
import com.michael.demos.base.design.facade.tea.type.TeaTypeEnum;
import com.michael.demos.base.design.facade.tea.type.impl.BlcTeaMaker;
import com.michael.demos.base.design.facade.tea.type.impl.LjTeaMaker;
import com.michael.demos.base.design.facade.tea.type.impl.WlTeaMaker;

import java.util.HashMap;
import java.util.Map;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 门面 - 茶馆
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 11:23
 */
public class TeaHouseFacade {

    private final String name;

    /** 所有制茶师 */
    private Map<TeaTypeEnum, TeaMaker> teaMap;

    private static final TeaTypeEnum DEFAULT_TEA_TYPE = TeaTypeEnum.WL;

    public TeaHouseFacade(String name) {
        this.name = name;
        initTeaMap();
    }

    /** 制作茶水 */
    public Teawater makeTea(TeaTypeEnum tea) {

        System.out.println(name + "来客人了");

        TeaMaker teaMaker = teaMap.get(tea);
        if (teaMaker == null) {
            teaMaker = teaMap.get(DEFAULT_TEA_TYPE);
        }
        return teaMaker.makeTea();
    }

    private void initTeaMap() {

        teaMap = new HashMap<>();

        teaMap.put(TeaTypeEnum.LJ, new LjTeaMaker());
        teaMap.put(TeaTypeEnum.BLC, new BlcTeaMaker());
        teaMap.put(TeaTypeEnum.WL, new WlTeaMaker());
    }
}

