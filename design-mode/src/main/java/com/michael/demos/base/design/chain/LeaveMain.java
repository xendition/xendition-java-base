package com.michael.demos.base.design.chain;

import com.michael.demos.base.design.chain.leave.LeaveRequest;
import com.michael.demos.base.design.chain.leave.handler.AbstractHandler;
import com.michael.demos.base.design.chain.leave.handler.impl.DirectorHandler;
import com.michael.demos.base.design.chain.leave.handler.impl.ManagerHandler;
import com.michael.demos.base.design.chain.leave.handler.impl.TopManagerHandler;

/**
 * 类功能描述:
 * <pre>
 *   责任链模式 - 客户端使用 - 请假请求测试
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:32
 */
public class LeaveMain {

    public static void main(String[] args) {

        AbstractHandler topManager = new TopManagerHandler();
        AbstractHandler manager = new ManagerHandler();
        AbstractHandler director = new DirectorHandler();

        // 创建责任链
        manager.setNext(topManager);
        director.setNext(manager);

        AbstractHandler handler = director;

        testLeaveRequest(handler, "小王", 2);
        testLeaveRequest(handler, "小王", 2);
        testLeaveRequest(handler, "小李", 4);
        testLeaveRequest(handler, "小李", 4);
        testLeaveRequest(handler, "小李", 4);
        testLeaveRequest(handler, "小张", 8);
        testLeaveRequest(handler, "小张", 8);
        testLeaveRequest(handler, "小张", 8);
        testLeaveRequest(handler, "小张", 8);

    }

    private static void testLeaveRequest(AbstractHandler handler, String name, int days) {
        boolean result = handler.process(new LeaveRequest(name, days));
        String resultStr = name + "请假" + days + "天 -> " + (result ? "成功" : "失败");
        System.out.println(resultStr + "\n");
    }

}
