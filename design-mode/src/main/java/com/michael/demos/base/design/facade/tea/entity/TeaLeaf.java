package com.michael.demos.base.design.facade.tea.entity;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 实体 - 泡茶用的茶叶
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 10:29
 */
public class TeaLeaf {

    /** 茶叶名称 */
    private String teaName;

    public TeaLeaf(String teaName) {
        this.teaName = teaName;
    }

    public String getTeaName() {
        return teaName;
    }

    public void setTeaName(String teaName) {
        this.teaName = teaName;
    }
}
