package com.michael.demos.base.design.observer.jdk;


import java.util.Observable;
import java.util.Observer;

/**
 * 类功能描述:
 * <pre>
 *   用户
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:45
 */
public class User implements Observer {

    private String name;

    public User(String name) {
        this.name = name;
    }

    @Override
    public void update(Observable provider, Object data) {
        News news = (News) data;
        System.out.println(name + " 接到新报纸 " + news);
    }
}
