package com.michael.demos.base.design.state.leave;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 17:58
 */
public class ManagerState extends State {
	@Override
	void handle() {
		System.out.println("经理审批通过，下一个总经理审批");
		context.setState(new TopManagerState());
	}
}
