package com.michael.demos.base.design.memento.single.whitebox;

/**
 * 类功能描述:
 * <pre>
 *  白箱模式实现备忘录 —— 备忘录角色类
 *  备忘录角色有如下责任：
 * 		1. 将发起人(Originator)对象的内部状态存储起来。备忘录可以根据发起人对象的判断来决定存储多少个发起人(Originator)对象的内部状态。
 * 		2. 备忘录可以保护其内容不被发起人(Originator)对象之外的任何对象所读取。
 *
 *  备忘录对象将发起人对象传入的状态存储起来。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:12
 */
public class Memento {

	private String state;

	public Memento(String state) {
		this.state = state;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
}
