package com.michael.demos.base.design.observer.jdk;

/**
 * 类功能描述:
 * <pre>
 *   新闻
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:34
 */
public class News {

	private String title;
	private String content;

	public News(String title, String content) {
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "News{" +
			   "title='" + title + '\'' +
			   ", content='" + content + '\'' +
			   '}';
	}
}
