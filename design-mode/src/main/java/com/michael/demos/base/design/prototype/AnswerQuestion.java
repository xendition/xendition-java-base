package com.michael.demos.base.design.prototype;

/**
 * 类功能描述:
 * <pre>
 *   问答题
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 16:49
 */
public class AnswerQuestion {
	/** 问题 */
	private String name;
	/** 答案 */
	private String key;

	public AnswerQuestion() {}

	public AnswerQuestion(String name, String key) {
		this.name = name;
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
