package com.michael.demos.base.design.flyweight;

import com.michael.demos.base.design.flyweight.trees.Forest;

import java.awt.*;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 16:13
 */
public class TreeMain {

    static int CANVAS_SIZE = 1000;
    static int TREES_TO_DRAW = 18000;
    static int TREE_TYPES = 3;

    public static void main(String[] args) {
        Forest forest = new Forest();
        for (int i = 0; i < Math.floor(TREES_TO_DRAW / TREE_TYPES); i++) {
            forest.plantTree(random(20, CANVAS_SIZE - 20), random(50, CANVAS_SIZE - 20),
                             "Summer Oak", Color.GREEN, "Oak texture stub"
            );
            forest.plantTree(random(20, CANVAS_SIZE - 20), random(50, CANVAS_SIZE - 20),
                             "Autumn Oak", Color.RED, "Autumn Oak texture stub"
            );

            forest.plantTree(random(20, CANVAS_SIZE - 20), random(50, CANVAS_SIZE - 20),
                             "xxx Oak", Color.BLUE, "xxx Oak texture stub"
            );
        }
        forest.setSize(CANVAS_SIZE, CANVAS_SIZE);
        forest.setVisible(true);

        System.out.println(TREES_TO_DRAW + " trees drawn");
        System.out.println("---------------------");
        System.out.println("Memory usage:");
        System.out.println("Tree size (8 bytes) * " + TREES_TO_DRAW);
        System.out.println("+ TreeTypes size (~30 bytes) * " + TREE_TYPES + "");
        System.out.println("---------------------");
        System.out.println("Total: " + ((TREES_TO_DRAW * 8 + TREE_TYPES * 30) / 1024 / 1024) +
                           "MB (instead of " + ((TREES_TO_DRAW * 38) / 1024 / 1024) + "MB)");
    }

    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
