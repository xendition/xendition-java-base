package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   形状
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:10
 */
public interface Shape {

    void draw();
}
