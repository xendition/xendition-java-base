package com.michael.demos.base.design.state.activity.service;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;

public interface ActivityService {

	/**
	 * 活动提审
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result arraignment(ActivityInfo info);

	/**
	 * 审核通过
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result checkPass(ActivityInfo info);

	/**
	 * 审核拒绝
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result checkRefuse(ActivityInfo info);

	/**
	 * 撤审撤销
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result checkRevoke(ActivityInfo info);

	/**
	 * 活动关闭
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result close(ActivityInfo info);

	/**
	 * 活动开启
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result open(ActivityInfo info);

	/**
	 * 活动执行
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	Result doing(ActivityInfo info);

	void add(ActivityInfo info);

	ActivityInfo queryById(Integer activityId);

	ActivityInfo updateById(ActivityInfo info);

	void removeById(Integer activityId);
}
