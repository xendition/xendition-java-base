package com.michael.demos.base.design.composite.goodstree;

/**
 * 类功能描述:
 * <pre>
 *   安全组合模式 —— 测试类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 17:08
 */
public class Main {

    public static void main(String[] args) {
        //定义所有的组合对象
        Composite root = new Composite("服装");
        Composite c1 = new Composite("女装");
        Composite c2 = new Composite("男装");

        //定义所有的叶子对象
        Leaf leaf1 = new Leaf("裙子");
        Leaf leaf2 = new Leaf("旗袍");
        Leaf leaf3 = new Leaf("衬衣");
        Leaf leaf4 = new Leaf("夹克");

        //按照树的结构来组合组合对象和叶子对象
        root.addChild(c1);
        root.addChild(c2);

        c1.addChild(leaf1);
        c1.addChild(leaf2);

        c2.addChild(leaf3);
        c2.addChild(leaf4);

        // 打印输出整棵树结构
        root.printStruct("");
    }
}
