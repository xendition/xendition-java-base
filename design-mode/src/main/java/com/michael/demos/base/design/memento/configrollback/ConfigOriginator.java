package com.michael.demos.base.design.memento.configrollback;

/** 发起人 */
public class ConfigOriginator {

	private ConfigFile configFile;

	public ConfigFile getConfigFile() {
		return configFile;
	}

	public void setConfigFile(ConfigFile configFile) {
		this.configFile = configFile;
	}

	public ConfigMemento saveMemento() {
		return new ConfigMemento(configFile);
	}

	public void getMemento(ConfigMemento memento) {
		this.configFile = memento.getConfigFile();
	}

}
