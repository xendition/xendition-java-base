package com.michael.demos.base.design.facade.tea.type.impl;

import com.michael.demos.base.design.facade.tea.entity.Man;
import com.michael.demos.base.design.facade.tea.entity.TeaLeaf;
import com.michael.demos.base.design.facade.tea.entity.Teawater;
import com.michael.demos.base.design.facade.tea.entity.Water;
import com.michael.demos.base.design.facade.tea.service.KettleService;
import com.michael.demos.base.design.facade.tea.service.TeasetService;
import com.michael.demos.base.design.facade.tea.type.TeaMaker;
import com.michael.demos.base.design.facade.tea.type.TeaTypeEnum;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/1 16:44
 */
public class WlTeaMaker implements TeaMaker {

    private final TeasetService teasetService = new TeasetService();
    private final KettleService kettleService = new KettleService();

    @Override
    public Teawater makeTea() {

        Man maker = new Man("招牌乌龙泡茶师");

        Water water = new Water();
        TeaLeaf teaLeaf = new TeaLeaf(TeaTypeEnum.WL.getTeaName());
        kettleService.waterBurning(maker, water, 5);

        return teasetService.makeTeaWater(maker, water, teaLeaf);
    }
}
