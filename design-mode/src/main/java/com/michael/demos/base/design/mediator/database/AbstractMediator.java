package com.michael.demos.base.design.mediator.database;

/**
 * 类功能描述:
 * <pre>
 *   中介
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:39
 */
public abstract class AbstractMediator {

	protected MysqlDatabase mysqlDatabase;
	protected RedisDatabase redisDatabase;
	protected EsDatabase esDatabase;

	public MysqlDatabase getMysqlDatabase() {
		return mysqlDatabase;
	}

	public void setMysqlDatabase(MysqlDatabase mysqlDatabase) {
		this.mysqlDatabase = mysqlDatabase;
	}

	public RedisDatabase getRedisDatabase() {
		return redisDatabase;
	}

	public void setRedisDatabase(RedisDatabase redisDatabase) {
		this.redisDatabase = redisDatabase;
	}

	public EsDatabase getEsDatabase() {
		return esDatabase;
	}

	public void setEsDatabase(EsDatabase esDatabase) {
		this.esDatabase = esDatabase;
	}

	public abstract void sync(DatabaseEnum databaseEnum, String data);
}
