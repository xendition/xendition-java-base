package com.michael.demos.base.design.mediator;

import com.michael.demos.base.design.mediator.database.*;

/**
 * 类功能描述:
 * <pre>
 *   Main
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:38
 */
public class DatabaseMain {

	public static void main(String[] args) {

		/* 建立中介关系 */
		AbstractMediator syncMediator = new SyncMediator();
		MysqlDatabase mysqlDatabase = new MysqlDatabase(syncMediator);
		RedisDatabase redisDatabase = new RedisDatabase(syncMediator);
		EsDatabase esDatabase = new EsDatabase(syncMediator);

		syncMediator.setMysqlDatabase(mysqlDatabase);
		syncMediator.setRedisDatabase(redisDatabase);
		syncMediator.setEsDatabase(esDatabase);


		System.out.println("\n---------mysql 添加数据，将同步到Redis和ES中-----------");
		mysqlDatabase.add("mysql");

		mysqlDatabase.select();
		redisDatabase.cache();
		esDatabase.count();

		System.out.println("\n---------Redis 添加数据，将不同步到其它数据库-----------");
		redisDatabase.add("Redis");

		mysqlDatabase.select();
		redisDatabase.cache();
		esDatabase.count();

		System.out.println("\n---------ES 添加数据，只同步到 Mysql-----------");
		esDatabase.add("ES");

		mysqlDatabase.select();
		redisDatabase.cache();
		esDatabase.count();
	}
}
