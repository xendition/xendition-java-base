package com.michael.demos.base.design.facade.tea.entity;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 实体 - 泡水
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 11:06
 */
public class Teawater {

    private String teaWater;

    public Teawater(String teaWater) {
        this.teaWater = teaWater;
    }

    public String getTeaWater() {
        return teaWater;
    }

    public void setTeaWater(String teaWater) {
        this.teaWater = teaWater;
    }
}
