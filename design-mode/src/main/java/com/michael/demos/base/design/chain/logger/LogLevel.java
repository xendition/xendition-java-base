package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:48
 */
public enum LogLevel {
	DEBUG, INFO, ERROR
}
