package com.michael.demos.base.design.flyweight.seckill;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 类功能描述:
 * <pre>
 *   享元模式 - 享元工厂 - 活动工厂
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 14:59
 */
public class ActivityFactory {

    static Map<Long, Activity> activityMap = new ConcurrentHashMap<>();

    public static Activity getActivity(Long id) {
        Activity activity = activityMap.get(id);
        if (null == activity) {
            activity = getInDB(id);
            activityMap.put(id, activity);
        }
        return activity;
    }

    public static Activity getInDB(Long Id) {
        // 模拟从实际业务应用从接口中获取活动信息
        Activity activity = new Activity();
        activity.setId(Id);
        activity.setName("所有图书1元秒杀");
        return activity;
    }
}
