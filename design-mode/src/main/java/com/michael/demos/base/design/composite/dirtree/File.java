package com.michael.demos.base.design.composite.dirtree;

/**
 * 类功能描述:
 * <pre>
 *   透明组合模式 —— 文件
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 16:40
 */
public class File extends Component {
    /** 文件名称 */
    private String name;
    /** 文件内容 */
    private String content;

    public File(String name, String content) {
        this.name = name;
        this.content = content;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void print() {
        System.out.println(this.getName());
    }

    @Override
    public String getContent() {
        return this.content;
    }
}
