package com.michael.demos.base.design.composite.decision.service.logic.impl;


import com.michael.demos.base.design.composite.decision.service.logic.BaseLogic;

import java.util.Map;

/** 根据用户性别过滤 */
public class UserGenderFilter extends BaseLogic {

    @Override
    public String matterValue(Long treeId, String userId, Map<String, String> decisionMatter) {
        return decisionMatter.get("gender");
    }
}
