package com.michael.demos.base.design.facade.tea.service;

import com.michael.demos.base.design.facade.tea.entity.Man;
import com.michael.demos.base.design.facade.tea.entity.TeaLeaf;
import com.michael.demos.base.design.facade.tea.entity.Teawater;
import com.michael.demos.base.design.facade.tea.entity.Water;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 子系统 - 茶具
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 11:04
 */
public class TeasetService {

    public Teawater makeTeaWater(Man who, Water water, TeaLeaf teaLeaf) {
        String teaWater = "一杯" + water.getCapacity()
                          + "ML 温度为 " + water.getTemperature()
                          + "℃的" + teaLeaf.getTeaName() + "茶";
        System.out.println(who.getName() + "泡了" + teaWater);
        return new Teawater(teaWater);
    }
}
