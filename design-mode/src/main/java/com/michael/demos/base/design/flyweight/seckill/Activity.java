package com.michael.demos.base.design.flyweight.seckill;

/**
 * 类功能描述:
 * <pre>
 *   享元模式 -  - 秒杀活动
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 14:55
 */
public class Activity {

    private Long id;
    private String name;
    private Stock stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Activity{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", stock=" + stock +
               '}';
    }
}
