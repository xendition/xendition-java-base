package com.michael.demos.base.design.mediator.database;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   Mysql
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:41
 */
public class MysqlDatabase extends AbstractDatabase {

	private List<String> dataset = new ArrayList<String>();

	public MysqlDatabase(AbstractMediator mediator) {
		super(mediator);
	}

	@Override
	public void addData(String data) {
		System.out.println("Mysql 添加数据：" + data);
		this.dataset.add(data);
	}

	@Override
	public void add(String data) {
		addData(data);
		// 数据同步作业交给中介者管理
		this.mediator.sync(DatabaseEnum.Mysql, data);
	}

	public void select() {
		System.out.println("Mysql 查询，数据：" + this.dataset.toString());
	}
}
