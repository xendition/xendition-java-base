package com.michael.demos.base.design.memento.historyself;


/**
 * 类功能描述:
 * <pre>
 *   自述历史模式实现备忘录 —— 发起人角色类
 *   发起人角色有如下责任：
 * 		1. 创建一个含有当前内部状态的备忘录对象
 * 		2. 使用备忘录对象存储其内部状态。
 *
 * 	发起人角色同时还要兼任负责人角色，也就是说她自己负责保持自己的备忘录对象。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:14
 */
public class Originator {

	private String state;

	/**
	 * 状态变更
	 *
	 * @param state
	 */
	public void changeState(String state) {
		this.state = state;
		System.out.println("状态改变为：" + this.state);
	}

	/**
	 * 工厂方法，返回一个新的备忘录对象
	 *
	 * @return
	 */
	public Memento createMemento() {
		return new Memento(this);
	}

	/**
	 * 将发起人状态恢复到备忘录对象所记录的状态上
	 *
	 * @param memento
	 */
	public void restoreMemento(MementoIF memento) {
		changeState(((Memento) memento).getState());
	}

	public class Memento implements MementoIF {

		private String state;

		/**
		 * 构造方法
		 *
		 * @param originator
		 */
		private Memento(Originator originator) {
			this.state = originator.state;
		}

		private String getState() {
			return this.state;
		}
	}
}
