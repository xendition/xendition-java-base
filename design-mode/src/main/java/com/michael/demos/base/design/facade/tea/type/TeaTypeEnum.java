package com.michael.demos.base.design.facade.tea.type;

/**
 * 类功能描述:
 * <pre>
 *   茶类型
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/1 16:34
 */
public enum TeaTypeEnum {

    LJ("西湖龙井"),
    BLC("碧螺春"),
    WL("招牌乌龙"),
    ;
    private final String teaName;

    public String getTeaName() {
        return teaName;
    }

    TeaTypeEnum(String teaName) {
        this.teaName = teaName;
    }
}
