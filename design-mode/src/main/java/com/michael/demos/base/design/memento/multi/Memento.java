package com.michael.demos.base.design.memento.multi;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *  多检查点实现备忘录 —— 备忘录角色类
 *  备忘录角色有如下责任：
 * 		1. 将发起人(Originator)对象的内部状态存储起来。备忘录可以根据发起人对象的判断来决定存储多少个发起人(Originator)对象的内部状态。
 * 		2. 备忘录可以保护其内容不被发起人(Originator)对象之外的任何对象所读取。
 *
 *  备忘录对象将发起人对象传入的状态存储起来。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:12
 */
public class Memento {

	private List<String> states;
	private int index;

	/**
	 * 构造方法
	 *
	 * @param states
	 * @param index
	 */
	public Memento(List<String> states, int index) {
		//该处需要注意，我们在这里重新构建了一个新的集合，拷贝状态集合到新的集合中，保证原有集合变化不会影响到我们记录的值
		this.states = new ArrayList<>(states);
		this.index = index;
	}

	public List<String> getStates() {
		return states;
	}

	public int getIndex() {
		return index;
	}
}
