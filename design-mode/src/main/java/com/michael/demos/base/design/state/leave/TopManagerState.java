package com.michael.demos.base.design.state.leave;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 17:58
 */
public class TopManagerState extends State {
	@Override
	void handle() {
		System.out.println("总经理审批通过");
		//审核通过之后的逻辑
		System.out.println("财务打款500元");
	}
}
