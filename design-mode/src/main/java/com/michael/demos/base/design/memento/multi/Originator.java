package com.michael.demos.base.design.memento.multi;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   多检查点实现备忘录 —— 发起人角色类
 *   发起人角色有如下责任：
 * 		1. 创建一个含有当前内部状态的备忘录对象
 * 		2. 使用备忘录对象存储其内部状态。
 *
 * 	发起人角色利用一个新创建的备忘录对象将自己的内部状态存储起来。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:14
 */
public class Originator {

	private List<String> states;
	//检查点序号
	private int index;

	/**
	 * 构造函数
	 */
	public Originator() {
		this.states = new ArrayList<>();
		index = 0;
	}

	/**
	 * 工厂方法，返回一个新的备忘录对象
	 *
	 * @return
	 */
	public Memento createMemento() {
		return new Memento(states, index);
	}

	/**
	 * 将发起人恢复到备忘录对象记录的状态上。
	 *
	 * @param memento
	 */
	public void restoreMemento(Memento memento) {
		this.states = memento.getStates();
		this.index = memento.getIndex();
	}

	/**
	 * 状态的赋值方法
	 *
	 * @param state
	 */
	public void setState(String state) {
		this.states.add(state);
		this.index++;
	}

	public List<String> getStates() {
		return states;
	}

	/**
	 * 辅助方法，打印所有状态
	 */
	public void pringStates() {
		System.out.println("当前检查点共有：" + states.size() + "个状态值");
		for (String state : states) {
			System.out.println(state);
		}
	}
}
