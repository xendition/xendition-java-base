package com.michael.demos.base.design.memento.single.blackbox;

/**
 * 类功能描述:
 * <pre>
 *   黑箱模式实现备忘录 —— 发起人角色类
 *   发起人角色有如下责任：
 * 		1. 创建一个含有当前内部状态的备忘录对象
 * 		2. 使用备忘录对象存储其内部状态。
 *
 * 	发起人角色利用一个新创建的备忘录对象将自己的内部状态存储起来。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:14
 */
public class Originator {

	private String state;

	/**
	 * 将发起人的状态恢复到备忘录对象所记录的状态
	 *
	 * @param memento
	 */
	public void restoreMemento(MementoIF memento) {
		this.state = ((Memento) memento).getState();
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
		System.out.println("当前状态：" + this.state);
	}

	/**
	 * 工厂方法，返回一个新的备忘录对象
	 *
	 * @return
	 */
	public MementoIF createMemento() {
		return new Memento(this.state);
	}

	private class Memento implements MementoIF {

		private String state;

		/**
		 * 构造方法
		 *
		 * @param state
		 */
		private Memento(String state) {
			this.state = state;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}
	}
}
