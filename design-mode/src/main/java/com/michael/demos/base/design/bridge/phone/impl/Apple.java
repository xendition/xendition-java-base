package com.michael.demos.base.design.bridge.phone.impl;

import com.michael.demos.base.design.bridge.phone.Brand;

/**
 * 类功能描述:
 * <pre>
 *   桥接模式 - 具体实现类 - 苹果手机
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/10 16:25
 */
public class Apple implements Brand {
    @Override
    public void open() {
        System.out.println("苹果手机开机..");
    }

    @Override
    public void close() {
        System.out.println("苹果手机关机..");
    }

    @Override
    public void call() {
        System.out.println("苹果手机打电话..");
    }
}
