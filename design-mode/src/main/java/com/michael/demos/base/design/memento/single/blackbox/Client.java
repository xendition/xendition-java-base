package com.michael.demos.base.design.memento.single.blackbox;


/**
 * 类功能描述:
 * <pre>
 *  黑箱模式实现备忘录 —— 客户端角色类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:19
 */
public class Client {

	/**
	 * 1. 客户端首先将发起人角色的状态设置为“On”；
	 * 2. 然后调用发起人角色的createMemento()方法，创建一个备忘录对象将发起人角色的状态存储起来(这个方法返回一个MementoIF接口，真实的数据类型为Originator内部类的Memento对象)。
	 * 3. 将备忘录对象存储到负责人对象中去，由于负责人对象存储的仅仅是MementoIF接口，因此无法获取备忘录对象内部存储的状态。
	 * 4. 将发起人对象的状态设置为“Off”。
	 * 5. 调用负责人对象的restoreMemento()方法将备忘录对象取出。注意，此时仅能或得到的返回结果为MementoIF接口，因此无法读取此对象的内部状态。
	 * 6. 调用发起人对象的retrieveMenento()方法将发起人对象的状态恢复到备忘录对象存储的状态上，也就是“On”状态。由于发起人对象的内部类Memento实现了MementoIF接口，这个内部类是传入的备忘录对象的真实类型，因此发起人对象可以利用内部类Memento的私有接口读出此对象的内部状态。
	 */
	public static void main(String[] args) {
		Originator originator = new Originator();
		Caretaker caretaker = new Caretaker();
		//改变发起人对象的状态
		originator.setState("On");
		//创建备忘录对象，并将发起人对象的状态存储起来
		caretaker.saveMemento(originator.createMemento());
		//修改发起人对象的状态
		originator.setState("Off");
		//恢复发起人对象的状态
		originator.restoreMemento(caretaker.retrieveMenento());

		//发起人对象的状态
		System.out.println("发起人对象的当前状态为：" + originator.getState());
	}
}
