package com.michael.demos.base.design.strategy.coupon;

import java.math.BigDecimal;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 优惠券策略接口
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 16:04
 */
public interface ICouponDiscountStrategy {

    BigDecimal discountAmount(BigDecimal orderPrice, BigDecimal couponPrice);
}
