package com.michael.demos.base.design.composite.awt;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 类功能描述:
 * <pre>
 *   安全组合模式 —— SWING 使用
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 16:51
 */
public class MyFrame extends JFrame {

	public MyFrame(String title) {
		super(title);
	}

	public static void main(String[] args) {
		MyFrame frame = new MyFrame("这是一个 Frame");

		// 定义三个构件，添加到Frame中去
		JButton button = new JButton("按钮 A");
		JLabel label = new JLabel("这是一个 AWT Label!");
		JTextField textField = new JTextField("这是一个 AWT TextField!");

		frame.add(button, BorderLayout.EAST);
		frame.add(label, BorderLayout.SOUTH);
		frame.add(textField, BorderLayout.NORTH);

		// 定义一个 Panel，在Panel中添加三个构件，然后再把Panel添加到Frame中去
		JPanel panel = new JPanel();
		panel.setBackground(Color.white);

		JLabel lable1 = new JLabel("用户名:");
		TextField textField1 = new TextField("请输入用户名", 20);
		JButton button1 = new JButton("确定");
		panel.add(lable1);
		panel.add(textField1);
		panel.add(button1);

		frame.add(panel, BorderLayout.CENTER);

		// 设置Frame的属性
		frame.setSize(500, 300);
		frame.setBackground(Color.orange);

		// 设置点击关闭事件
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		frame.setVisible(true);
	}
}
