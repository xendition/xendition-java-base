package com.michael.demos.base.design.observer.newspaper;

/**
 * 类功能描述:
 * <pre>
 *   被观察者接口定义 —— 生产者
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:29
 */
public interface Provider {

    void register(Consumer consumer);

    void remove(Consumer consumer);

    void send(News model);
}
