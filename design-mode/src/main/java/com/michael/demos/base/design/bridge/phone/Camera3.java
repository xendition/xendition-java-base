package com.michael.demos.base.design.bridge.phone;

/**
 * 类功能描述:
 * <pre>
 *   桥接模式 - 扩展抽象类 - 三摄手机
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/10 16:29
 */
public class Camera3 extends Phone{

    public Camera3(Brand brand) {
        super(brand);
    }


    @Override
    public void open() {
        super.open();
        System.out.println("三摄像头手机的开机相关操作");
    }

    @Override
    public void close() {
        super.close();
        System.out.println("三摄像头手机的关机相关操作");
    }

    @Override
    public void call() {
        super.call();
        System.out.println("三摄像头手机的打电话相关操作");
    }
}
