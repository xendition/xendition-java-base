package com.michael.demos.base.design.observer;

import com.michael.demos.base.design.observer.newspaper.NewsOffice;
import com.michael.demos.base.design.observer.newspaper.Provider;
import com.michael.demos.base.design.observer.newspaper.User;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 16:27
 */
public class NewspaperMain {

    public static void main(String[] args) {
        Provider provider = new NewsOffice();
        User user;
        for (int i = 0; i < 10; i++) {
            user = new User("user:" + i);
            provider.register(user);
        }
    }
}
