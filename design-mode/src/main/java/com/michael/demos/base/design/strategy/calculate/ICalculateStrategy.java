package com.michael.demos.base.design.strategy.calculate;

/**
 * 类功能描述:
 * <pre>
 *   策略模式——策略接口
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/31 9:13
 */
public interface ICalculateStrategy {

    Number doCalculate(int first, int second);
}
