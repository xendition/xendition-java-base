package com.michael.demos.base.design.facade.tea.type;

import com.michael.demos.base.design.facade.tea.entity.Teawater;

/**
 * 类功能描述:
 * <pre>
 *   制茶师
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/9/1 16:33
 */
public interface TeaMaker {

    Teawater makeTea();
}
