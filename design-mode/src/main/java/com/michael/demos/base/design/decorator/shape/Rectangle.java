package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   长方形
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:10
 */
public class Rectangle implements Shape {

    @Override
    public void draw() {
        System.out.println("画了一个长方形");
    }
}
