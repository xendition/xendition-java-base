package com.michael.demos.base.design.facade.tea.entity;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 实体 - 喝茶的人
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 11:08
 */
public class Man {

    private String name;

    public Man(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /** 喝茶 */
    public void drinkTea(Teawater teawater) {
        System.out.println(name + "喝了" + teawater.getTeaWater());
    }
}
