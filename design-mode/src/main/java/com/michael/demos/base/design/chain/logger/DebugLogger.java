package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:46
 */
public final class DebugLogger extends AbstractLogger {

	DebugLogger() {
		super(LogLevel.DEBUG);
	}

	@Override
	protected void write(String message) {
		System.out.println("\u001B[32m[DEBUG] " + message);
	}
}
