package com.michael.demos.base.design.memento.multi;

/**
 * 类功能描述:
 * <pre>
 *  多检查点实现备忘录 —— 客户端角色类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:19
 */
public class Client {

	/**
	 * 1. 将发起人的状态设置为某个有效状态，发起人会记录当前已经设置的状态值列表
	 * 2. 调用负责人角色的createMemento()方法创建备忘录角色存储点，在该方法中，会调用发起人角色的createMemento()创建一个备忘录对象，记录当时发起人对象中的状态值列表和序号。然后负责人角色对象将发起人对象返回的备忘录对象存储起来。
	 */
	public static void main(String[] args) {

		Originator originator = new Originator();
		Caretaker caretaker = new Caretaker(originator);
		//改变状态
		originator.setState("State 0");
		//建立一个检查点
		caretaker.createMemento();
		//改变状态
		originator.setState("State 1");
		//建立一个检查点
		caretaker.createMemento();
		//改变状态
		originator.setState("State 2");
		;
		//建立一个检查点
		caretaker.createMemento();
		//改变状态
		originator.setState("State 3");
		//建立一个检查点
		caretaker.createMemento();
		//改变状态
		originator.setState("State 4");
		//建立一个检查点
		caretaker.createMemento();
		//打印出所有的检查点
		originator.pringStates();
		System.out.println("---------恢复状态到某一个检查点----------");
		//恢复到第二个检查点
		caretaker.restoreMemento(1);
		//打印出所有的检查点.
		originator.pringStates();
	}
}
