package com.michael.demos.base.design.adapter;

/**
 * 类功能描述:
 * <pre>
 *   适配器示例 —— 接口适配器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:39
 */
public class AdapterDemo3 {
	public static void main(String[] args) {
		AbsAdapter absAdapter = new AbsAdapter() {

			/**
			 * 只需要去覆盖我们需要使用的接口方法
			 */
			@Override
			public void m1() {
				System.out.println("使用类m1的方法");
			}

		};
		absAdapter.m1();
	}
}

interface Interface4 {

	void m1();

	void m2();

	void m3();

	void m4();

}

abstract class AbsAdapter implements Interface4 {
	@Override
	public void m1() {}

	@Override
	public void m2() {}

	@Override
	public void m3() {}

	@Override
	public void m4() {}
}
