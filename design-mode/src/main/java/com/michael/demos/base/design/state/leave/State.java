package com.michael.demos.base.design.state.leave;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 17:54
 */
public abstract class State {

	protected Context context;

	abstract void handle();

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
}
