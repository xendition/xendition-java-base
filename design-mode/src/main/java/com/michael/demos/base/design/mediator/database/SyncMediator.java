package com.michael.demos.base.design.mediator.database;

/**
 * 类功能描述:
 * <pre>
 *   具体的中介 —— 代码复杂性将在这里统一处理
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:44
 */
public class SyncMediator extends AbstractMediator {

	@Override
	public void sync(DatabaseEnum databaseEnum, String data) {
		if (DatabaseEnum.Mysql.equals(databaseEnum)) {
			// mysql 同步到 redis 和 Elasticsearch
			this.redisDatabase.addData(data);
			this.esDatabase.addData(data);
		} else if (DatabaseEnum.Redis.equals(databaseEnum)) {
			// redis 缓存同步，不需要同步到其他数据库
		} else if (DatabaseEnum.Elasticsearch.equals(databaseEnum)) {
			// Elasticsearch 同步到 Mysql
			this.mysqlDatabase.addData(data);
		}
	}
}
