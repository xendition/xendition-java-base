package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   抽象的日志记录器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:41
 */
public abstract class AbstractLogger {

	protected LogLevel level;

	//责任链中的下一个元素
	protected AbstractLogger nextLogger;

	public AbstractLogger() {}

	public AbstractLogger(LogLevel level) {
		this.level = level;
	}

	public void setNextLogger(AbstractLogger nextLogger) {
		this.nextLogger = nextLogger;
	}

	public boolean isDebug() {
		return this.level == LogLevel.DEBUG;
	}

	public boolean isInfo() {
		return this.level == LogLevel.INFO;
	}

	public boolean isError() {
		return this.level == LogLevel.ERROR;
	}

	public void debug(String msg) {
		this.doLog(LogLevel.DEBUG, msg);
	}

	public void info(String msg) {
		this.doLog(LogLevel.INFO, msg);
	}

	public void error(String msg) {
		this.doLog(LogLevel.ERROR, msg);
	}

	private void doLog(LogLevel level, String message) {
		if (this.level.ordinal() >= level.ordinal()) {
			write(message);
		}
		if (nextLogger != null) {
			nextLogger.doLog(level, message);
		}
	}

	/** 打印日志 */
	abstract protected void write(String message);
}
