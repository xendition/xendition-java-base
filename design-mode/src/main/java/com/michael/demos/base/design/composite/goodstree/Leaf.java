package com.michael.demos.base.design.composite.goodstree;

/**
 * 类功能描述:
 * <pre>
 *   安全组合模式 —— 非容器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 17:07
 */
public class Leaf extends Component {

    /**
     * 叶子对象的名字
     */
    private String name;

    public Leaf(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 输出叶子对象的结构，叶子对象没有子对象，也就是输出叶子对象的名字
     *
     * @param preStr 前缀，主要是按照层级拼接的空格，实现向后缩进
     */
    @Override
    public void printStruct(String preStr) {
        System.out.println(preStr + "-" + name);
    }
}
