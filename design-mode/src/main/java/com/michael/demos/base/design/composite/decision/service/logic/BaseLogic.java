package com.michael.demos.base.design.composite.decision.service.logic;

import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeLinkVO;

import java.util.List;
import java.util.Map;

/**
 * 类功能描述:
 * <pre>
 *   决策抽象类提供基础服务
 *   在抽象方法中实现了接口方法，同时定义了了基本的决策⽅方法； 1、2、3、4、5 ， 等于、小于、大于、小于等于、大于等于的判断逻辑。
 *   同时定义了抽象方法，让每一个实现接口的类都必须按照规则提供决策值这个决策值用于做逻辑比对。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 15:19
 */
public abstract class BaseLogic implements LogicFilter {

    @Override
    public Long filter(String matterValue, List<TreeNodeLinkVO> treeNodeLinkVOList) {
        for (TreeNodeLinkVO nodeLine : treeNodeLinkVOList) {
            if (decisionLogic(matterValue, nodeLine)) {
                return nodeLine.getNodeIdTo();
            }
        }
        return 0L;
    }

    @Override
    public abstract String matterValue(Long treeId, String userId, Map<String, String> decisionMatter);

    private boolean decisionLogic(String matterValue, TreeNodeLinkVO nodeLink) {
        switch (nodeLink.getRuleLimitType()) {
            case 1:
                return matterValue.equals(nodeLink.getRuleLimitValue());
            case 2:
                return Double.parseDouble(matterValue) > Double.parseDouble(nodeLink.getRuleLimitValue());
            case 3:
                return Double.parseDouble(matterValue) < Double.parseDouble(nodeLink.getRuleLimitValue());
            case 4:
                return Double.parseDouble(matterValue) <= Double.parseDouble(nodeLink.getRuleLimitValue());
            case 5:
                return Double.parseDouble(matterValue) >= Double.parseDouble(nodeLink.getRuleLimitValue());
            default:
                return false;
        }
    }
}
