package com.michael.demos.base.design.composite.decision.model.vo;

import java.util.List;

/**
 * 规则树节点信息
 */
public class TreeNodeVO {

    /** 规则树ID */
    private Long treeId;
    /** 规则树节点ID */
    private Long treeNodeId;
    /** 节点类型；1子叶、2结果 */
    private Integer nodeType;
    /** 节点值[nodeType=2]；结果值 */
    private String nodeValue;
    /** 规则Key */
    private String ruleKey;
    /** 规则描述 */
    private String ruleDesc;
    /** 节点链路 */
    private List<TreeNodeLinkVO> treeNodeLinkVOList;

    public Long getTreeId() {
        return treeId;
    }

    public void setTreeId(Long treeId) {
        this.treeId = treeId;
    }

    public Long getTreeNodeId() {
        return treeNodeId;
    }

    public void setTreeNodeId(Long treeNodeId) {
        this.treeNodeId = treeNodeId;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public String getNodeValue() {
        return nodeValue;
    }

    public void setNodeValue(String nodeValue) {
        this.nodeValue = nodeValue;
    }

    public String getRuleKey() {
        return ruleKey;
    }

    public void setRuleKey(String ruleKey) {
        this.ruleKey = ruleKey;
    }

    public String getRuleDesc() {
        return ruleDesc;
    }

    public void setRuleDesc(String ruleDesc) {
        this.ruleDesc = ruleDesc;
    }

    public List<TreeNodeLinkVO> getTreeNodeLinkVOList() {
        return treeNodeLinkVOList;
    }

    public void setTreeNodeLinkVOList(List<TreeNodeLinkVO> treeNodeLinkVOList) {
        this.treeNodeLinkVOList = treeNodeLinkVOList;
    }

    @Override
    public String toString() {
        return "TreeNodeVO{" +
               "treeId=" + treeId +
               ", treeNodeId=" + treeNodeId +
               ", nodeType=" + nodeType +
               ", nodeValue='" + nodeValue + '\'' +
               ", ruleKey='" + ruleKey + '\'' +
               ", ruleDesc='" + ruleDesc + '\'' +
               ", treeNodeLinkVOList=" + treeNodeLinkVOList +
               '}';
    }
}
