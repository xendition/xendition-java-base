package com.michael.demos.base.design.composite.goodstree;

/**
 * 类功能描述:
 * <pre>
 *   安全组合模式 —— 抽象构建
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/26 17:05
 */
public abstract class Component {
    /**
     * 输出组件自身的名称
     */
    public abstract void printStruct(String preStr);
}
