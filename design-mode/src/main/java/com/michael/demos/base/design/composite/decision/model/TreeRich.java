package com.michael.demos.base.design.composite.decision.model;


import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeVO;
import com.michael.demos.base.design.composite.decision.model.vo.TreeRootVO;

import java.util.Map;

/**
 * 规则树聚合
 */
public class TreeRich {

    /** 树根信息 */
    private TreeRootVO treeRoot;
    /** 树节点ID -> 子节点 */
    private Map<Long, TreeNodeVO> treeNodeMap;

    public TreeRich(TreeRootVO treeRoot, Map<Long, TreeNodeVO> treeNodeMap) {
        this.treeRoot = treeRoot;
        this.treeNodeMap = treeNodeMap;
    }

    public TreeRootVO getTreeRoot() {
        return treeRoot;
    }

    public void setTreeRoot(TreeRootVO treeRoot) {
        this.treeRoot = treeRoot;
    }

    public Map<Long, TreeNodeVO> getTreeNodeMap() {
        return treeNodeMap;
    }

    public void setTreeNodeMap(
            Map<Long, TreeNodeVO> treeNodeMap
    ) {
        this.treeNodeMap = treeNodeMap;
    }

    @Override
    public String toString() {
        return "TreeRich{" +
               "treeRoot=" + treeRoot +
               ", treeNodeMap=" + treeNodeMap +
               '}';
    }
}
