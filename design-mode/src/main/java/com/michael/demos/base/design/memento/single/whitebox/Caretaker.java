package com.michael.demos.base.design.memento.single.whitebox;

/**
 * 类功能描述:
 * <pre>
 *  白箱模式实现备忘录 —— 负责人角色类
 * 	负责人角色有如下责任：
 * 		1. 负责保存备忘录对象。
 * 		2. 不检查备忘录对象的内容
 *
 * 	负责人角色类负责保存备忘录对象，但是从不修改（甚至不查看）备忘录对象的内容。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:17
 */
public class Caretaker {

	private Memento memento;

	/**
	 * 备忘录的取值方法
	 *
	 * @return
	 */
	public Memento retrieveMenento() {
		return this.memento;
	}

	/**
	 * 备忘录的赋值方法
	 *
	 * @param memento
	 */
	public void saveMemento(Memento memento) {
		this.memento = memento;
	}
}
