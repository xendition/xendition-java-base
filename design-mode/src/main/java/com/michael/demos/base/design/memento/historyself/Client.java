package com.michael.demos.base.design.memento.historyself;


/**
 * 类功能描述:
 * <pre>
 *  自述历史模式实现备忘录 —— 客户端角色类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:19
 */
public class Client {

	public static void main(String[] args) {
		Originator originator = new Originator();
		//修改状态
		originator.changeState("State 0");
		//创建备忘录
		MementoIF memento = originator.createMemento();
		//修改状态
		originator.changeState("State 1");
		//按照备忘录对象存储的状态恢复发起人对象的状态
		originator.restoreMemento(memento);
	}
}
