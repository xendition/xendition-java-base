package com.michael.demos.base.design.flyweight.seckill;

/**
 * 类功能描述:
 * <pre>
 *   享元模式 -  - 库存
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 14:57
 */
public class Stock {

    /** 库存总量 */
    private int total;
    /** 库存已用 */
    private int used;

    public Stock(int total, int used) {
        this.total = total;
        this.used = used;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }

    @Override
    public String toString() {
        return "Stock{" +
               "total=" + total +
               ", used=" + used +
               '}';
    }
}
