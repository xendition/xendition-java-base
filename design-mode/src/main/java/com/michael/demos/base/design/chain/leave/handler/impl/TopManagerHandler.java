package com.michael.demos.base.design.chain.leave.handler.impl;

import com.michael.demos.base.design.chain.leave.LeaveRequest;
import com.michael.demos.base.design.chain.leave.handler.AbstractHandler;

import java.util.Random;

/**
 * 类功能描述:
 * <pre>
 *   责任链模式 - 具体处理类 - 总经理审批
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 16:49
 */
public class TopManagerHandler extends AbstractHandler {

    public TopManagerHandler() {
        super("张三");
    }

    @Override
    public boolean process(LeaveRequest leaveRequest) {

        Random random = new Random();
        int nextInt = random.nextInt(100);
        // 是否同意
        boolean result = (nextInt % 2) == 0;
        System.out.println("总经理<" + name + "> 审批结果 ：" + (result ? "同意" : "不同意"));
        return result;
    }
}
