package com.michael.demos.base.design.state.activity.service.impl.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.Result;

/**
 * 活动状态；待审核
 */
public class CheckState implements State {

//	@Override
//	public Result arraignment(ActivityInfo info) {
//		return new Result(ERROR_CODE, "待审核状态不可重复提审");
//	}

	@Override
	public Result checkPass(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Pass);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 审核通过");
	}

	@Override
	public Result checkRefuse(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Refuse);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 审核拒绝");
	}

	@Override
	public Result checkRevoke(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Editing);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 编辑中");
	}

	@Override
	public Result close(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Close);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 已关闭");
	}

//	@Override
//	public Result open(ActivityInfo info) {
//		return new Result(ERROR_CODE, "非关闭活动不可开启");
//	}
//
//	@Override
//	public Result doing(ActivityInfo info) {
//		return new Result(ERROR_CODE, "待审核活动不可执行活动中变更");
//	}
}
