package com.michael.demos.base.design.chain.leave;

/**
 * 类功能描述:
 * <pre>
 *   责任链模式 - 责任传递对象 - 请假请求
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 16:46
 */
public class LeaveRequest {

	private String name;
	private Integer days;

	public LeaveRequest(String name, Integer days) {
		System.out.println(name + " : 我要请假" + days + "天...");
		this.name = name;
		this.days = days;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}
}
