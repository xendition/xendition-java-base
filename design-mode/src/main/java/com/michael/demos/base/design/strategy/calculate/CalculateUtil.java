package com.michael.demos.base.design.strategy.calculate;

import com.michael.demos.base.design.strategy.calculate.impl.AddStrategy;
import com.michael.demos.base.design.strategy.calculate.impl.MultiplyStrategy;

/**
 * 类功能描述:
 * <pre>
 *   策略模式——工具类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/31 9:17
 */
public final class CalculateUtil {

    public static Number add(int first, int second) {
        return new CalculateService(new AddStrategy()).doCalculate(first, second);
    }

    public static Number multiply(int first, int second) {
        return new CalculateService(new MultiplyStrategy()).doCalculate(first, second);
    }

}
