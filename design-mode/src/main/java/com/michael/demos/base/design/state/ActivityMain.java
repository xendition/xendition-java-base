package com.michael.demos.base.design.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.ActivityService;
import com.michael.demos.base.design.state.activity.service.impl.ActivityServiceImpl;

import java.util.Date;

/**
 * 类功能描述:
 * <pre>
 *   状态模式 - 测试类 - 活动审批
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 14:00
 */
public class ActivityMain {

    private static final ActivityService activityService = new ActivityServiceImpl();

    public static void main(String[] args) {
        testAdd();
        testFlow();
    }

    public static void testAdd() {
        ActivityInfo info = new ActivityInfo(1, "活动A", StatusEnum.Editing, new Date(), new Date());
        activityService.add(info);
        info = activityService.queryById(1);
        System.out.println(info);
    }

    public static void testFlow() {

        ActivityInfo info = activityService.queryById(1);

        // 正规操作
        System.out.println(activityService.arraignment(info));
        System.out.println(activityService.checkPass(info));
        System.out.println(activityService.checkRefuse(info));
        System.out.println(activityService.checkRevoke(info));
        System.out.println(activityService.checkPass(info));
        System.out.println(activityService.doing(info));
        System.out.println(activityService.close(info));
        System.out.println(activityService.open(info));

        // 错误操作
        System.out.println("----------------------------------------");
        System.out.println(activityService.checkPass(info));
        System.out.println(activityService.arraignment(info));
        System.out.println(activityService.checkRevoke(info));
    }
}
