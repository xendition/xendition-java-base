package com.michael.demos.base.design.mediator.database;

/**
 * 类功能描述:
 * <pre>
 *   使用中介模式来处理数据同步
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:39
 */
public abstract class AbstractDatabase {

	public static final String MYSQL = "mysql";
	public static final String REDIS = "redis";
	public static final String ELASTICSEARCH = "elasticsearch";

	/** 中介者 */
	protected AbstractMediator mediator;

	public AbstractDatabase(AbstractMediator mediator) {
		this.mediator = mediator;
	}

	public abstract void addData(String data);

	public abstract void add(String data);
}
