package com.michael.demos.base.design.state.leave;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 17:55
 */
public class Context {
	private State state;

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
		this.state.setContext(this);
	}

	public void request() {
		this.state.handle();
	}
}
