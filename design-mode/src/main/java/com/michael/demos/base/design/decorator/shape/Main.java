package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   测试类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:13
 */
public class Main {

    public static void main(String[] args) {

        Shape circle = new Circle();
        //ShapeDecorator redCircle = new RedShapeDecorator(new Circle());
        //ShapeDecorator redRectangle = new RedShapeDecorator(new Rectangle());
        Shape redCircle = new RedShapeDecorator(new Circle());
        Shape redRectangle = new RedShapeDecorator(new Rectangle());

        Shape bigRedCircle = new BigShapeDecorator(new RedShapeDecorator(new Circle()));

        System.out.println("Circle with normal border");
        circle.draw();

        System.out.println("\nCircle of red border");
        redCircle.draw();

        System.out.println("\nRectangle of red border");
        redRectangle.draw();

        System.out.println("\nCircle of red border and reset Size");
        bigRedCircle.draw();
    }
}
