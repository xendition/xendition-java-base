package com.michael.demos.base.design.chain.logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 16:04
 */
public class Logger {
	private Logger() {}

	public static AbstractLogger getInstance() {
		return Logger.Inner.instance;
	}

	private static class Inner {
		private static final AbstractLogger instance = new ErrorLogger();
	}
}

