package com.michael.demos.base.design.facade.tea.entity;

/**
 * 类功能描述:
 * <pre>
 *   外观模式 - 实体 - 泡茶用的水
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/28 10:28
 */
public class Water {

    /** 温度(℃) */
    private int temperature;
    /** 容量(ML) */
    private int capacity;

    public Water() {
        this.temperature = 0;
        this.capacity = 300;
    }

    public Water(int temperature, int capacity) {
        this.temperature = temperature;
        this.capacity = capacity;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
