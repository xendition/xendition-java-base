package com.michael.demos.base.design.chain.leave.handler;

import com.michael.demos.base.design.chain.leave.LeaveRequest;

import java.util.Random;

/**
 * 类功能描述:
 * <pre>
 *   责任链模式 - 核心类 - 责任链抽象定义
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 16:47
 */
public abstract class AbstractHandler {

    protected static final Integer LEVEL1 = 3;
    protected static final Integer LEVEL2 = 7;

    protected static final Random RANDOM = new Random();

    /** 当前处理人名称 */
    protected String name;

    /** 下一个处理人 */
    protected AbstractHandler next;

    protected AbstractHandler() {}

    protected AbstractHandler(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AbstractHandler getNext() {
        return next;
    }

    public void setNext(AbstractHandler next) {
        this.next = next;
    }

    /** 实际处理方法,由子类实现 */
    public abstract boolean process(LeaveRequest leaveRequest);
}
