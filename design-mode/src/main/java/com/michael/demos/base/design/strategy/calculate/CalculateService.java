package com.michael.demos.base.design.strategy.calculate;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 计算服务类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 14:18
 */
public class CalculateService {

    private ICalculateStrategy calculateStrategy;

    public CalculateService(ICalculateStrategy calculateStrategy) {
        this.calculateStrategy = calculateStrategy;
    }

    public Number doCalculate(int first, int second) {
        return calculateStrategy.doCalculate(first, second);
    }
}
