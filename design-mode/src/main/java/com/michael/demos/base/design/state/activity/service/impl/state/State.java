package com.michael.demos.base.design.state.activity.service.impl.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.service.Result;

public interface State {

	String SUCCESS_CODE = "200";
	String ERROR_CODE = "405";

	/**
	 * 活动提审
	 *
	 * @param info 活动
	 * @return 执行结果
	 */
	default Result arraignment(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可提审<" + info.getStatusEnum() + ">");
	}

	/**
	 * 审核通过
	 *
	 * @return 执行结果
	 */
	default Result checkPass(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可审核通过<" + info.getStatusEnum() + ">");
	}

	/**
	 * 审核拒绝
	 *
	 * @return 执行结果
	 */
	default Result checkRefuse(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可审核拒绝<" + info.getStatusEnum() + ">");
	}

	/**
	 * 撤审撤销
	 *
	 * @return 执行结果
	 */
	default Result checkRevoke(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可撤审撤销<" + info.getStatusEnum() + ">");
	}

	/**
	 * 活动关闭
	 *
	 * @return 执行结果
	 */
	default Result close(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可关闭活动<" + info.getStatusEnum() + ">");
	}

	/**
	 * 活动开启
	 *
	 * @return 执行结果
	 */
	default Result open(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可开启活动<" + info.getStatusEnum() + ">");
	}

	/**
	 * 活动执行
	 *
	 * @return 执行结果
	 */
	default Result doing(ActivityInfo info) {
		return new Result(ERROR_CODE, "当前状态不可执行活动<" + info.getStatusEnum() + ">");
	}

}
