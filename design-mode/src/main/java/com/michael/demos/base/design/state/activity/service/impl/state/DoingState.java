package com.michael.demos.base.design.state.activity.service.impl.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.Result;

/**
 * 活动状态；活动中
 */
public class DoingState implements State {

//    @Override
//    public Result arraignment(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可提审");
//    }
//
//    @Override
//    public Result checkPass(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可审核通过");
//    }
//
//    @Override
//    public Result checkRefuse(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可审核拒绝");
//    }
//
//    @Override
//    public Result checkRevoke(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可撤销审核");
//    }

    @Override
    public Result close(ActivityInfo info) {
        info.setStatusEnum(StatusEnum.Close);
        return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 已关闭");
    }

//    @Override
//    public Result open(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可开启");
//    }
//
//    @Override
//    public Result doing(ActivityInfo info) {
//        return new Result(ERROR_CODE, "活动中不可重复执行");
//    }

}
