package com.michael.demos.base.design.chain;

import com.michael.demos.base.design.chain.logger.AbstractLogger;
import com.michael.demos.base.design.chain.logger.Logger;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/20 15:50
 */
public class LoggerMain {

	public static void main(String[] args) {

		System.out.println("开始");
		AbstractLogger logger = Logger.getInstance();

		logger.debug("1111111111111111111");
		logger.info("22222222222222222222222");
		logger.error("3333333333333333333333");


	}
}
