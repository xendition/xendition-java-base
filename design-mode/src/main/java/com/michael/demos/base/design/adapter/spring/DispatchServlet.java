package com.michael.demos.base.design.adapter.spring;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   Spring MVC 中的 DispatchServlet
 *
 * 		1）Spring定义了一个适配接口，使得每一种Controller有一种对应的适配器实现类
 * 		2）适配器代替 Controller 执行相应的方法
 * 		3）扩展Controller时，只需要增加一个适配器类就完成类SpringMVC的扩展类
 *
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:49
 */
public class DispatchServlet {

	public static List<HandlerAdapter> handlerAdapters = new ArrayList<HandlerAdapter>();

	public DispatchServlet() {
		handlerAdapters.add(new AnnotationHandlerAdapter());
		handlerAdapters.add(new HttpHandlerAdapter());
		handlerAdapters.add(new SimpleHandlerAdapter());
	}

	public void doDispatch() {
		// 此处模拟SpringMVC从request取handler的对象，
		// 适配器可以获取到希望的Controller
		// HttpController controller = new HttpController（）；
		AnnotationController controller = new AnnotationController();
		// SimpleController controller = new SimpleController();
		// 得到对应适配器
		HandlerAdapter adapter = getHandler(controller);
		// 通过适配器执行对应的Controller对应方法
		adapter.handle(controller);
	}

	private HandlerAdapter getHandler(AnnotationController controller) {
		for (HandlerAdapter adapter : this.handlerAdapters) {
			if (adapter.supports(controller)) {
				return adapter;
			}
		}
		return null;
	}

	public static void main(String[] args) {
		new DispatchServlet().doDispatch();
	}
}
