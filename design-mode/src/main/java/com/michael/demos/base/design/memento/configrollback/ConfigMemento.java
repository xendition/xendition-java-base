package com.michael.demos.base.design.memento.configrollback;

/**
 * 配置文件备忘录 —— 相当于原始类扩展
 */
public class ConfigMemento {

	private ConfigFile configFile;

	public ConfigMemento(ConfigFile configFile) {
		this.configFile = configFile;
	}

	public ConfigFile getConfigFile() {
		return configFile;
	}

	public void setConfigFile(ConfigFile configFile) {
		this.configFile = configFile;
	}

}
