package com.michael.demos.base.design.adapter;

/**
 * 类功能描述:
 * <pre>
 *   适配器示例 —— 对象适配器
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:34
 */
public class AdapterDemo {
	public static void main(String[] args) {
		System.out.println(" === 对象适配器模式 ===");
		Phone phone = new Phone();
		phone.charging(new VoltageAdapter(new Voltage220V()));
	}
}

class Phone {
	/**
	 * 充电
	 *
	 * @param iVoltage5V
	 */
	public void charging(IVoltage5V iVoltage5V) {
		if (iVoltage5V.output5V() == 5) {
			System.out.println("电压为5V，可以充电～～");
		} else if (iVoltage5V.output5V() > 5) {
			System.out.println("电压大于5V，不能充电～～");
		}
	}
}


/**
 * 类功能描述:
 * <pre>
 * 适配接口
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:31
 */
interface IVoltage5V {
	int output5V();
}


/**
 * 类功能描述:
 * <pre>
 * 被适配的类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:31
 */
class Voltage220V {

	/**
	 * 输出220V的电压
	 */
	public int output220V() {
		int src = 220;
		System.out.println("电压 = " + src + "伏");
		return src;
	}
}


/**
 * 类功能描述:
 * <pre>
 *   适配器类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/17 15:32
 */
class VoltageAdapter implements IVoltage5V {
	/**
	 * 关联关系 - 聚合
	 */
	private Voltage220V voltage220V;

	/**
	 * 通过构造器，传入一个Voltage220V 实例
	 *
	 * @param voltage220v
	 */
	public VoltageAdapter(Voltage220V voltage220v) {
		this.voltage220V = voltage220v;
	}

	@Override
	public int output5V() {
		int dstV = 0;
		if (voltage220V != null) {
			// 获取220V 电压
			int src = voltage220V.output220V();
			System.out.println("使用对象适配器，进行适配～～");
			dstV = src / 44;
			System.out.println("适配完成，输出的电压为 = " + dstV);
		}
		return dstV;
	}
}