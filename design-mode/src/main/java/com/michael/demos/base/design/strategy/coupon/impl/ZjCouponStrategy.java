package com.michael.demos.base.design.strategy.coupon.impl;

import com.michael.demos.base.design.strategy.coupon.ICouponDiscountStrategy;

import java.math.BigDecimal;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 优惠券策略实现类 —— 直减券
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 16:28
 */
public class ZjCouponStrategy implements ICouponDiscountStrategy {

    @Override
    public BigDecimal discountAmount(BigDecimal orderPrice, BigDecimal couponPrice) {
        // 直减券
        if (orderPrice.compareTo(couponPrice) > 0) {
            return orderPrice.subtract(couponPrice);
        }
        return BigDecimal.ZERO;
    }
}
