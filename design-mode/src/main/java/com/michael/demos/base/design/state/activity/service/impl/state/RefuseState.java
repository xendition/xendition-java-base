package com.michael.demos.base.design.state.activity.service.impl.state;

import com.michael.demos.base.design.state.activity.entity.ActivityInfo;
import com.michael.demos.base.design.state.activity.enums.StatusEnum;
import com.michael.demos.base.design.state.activity.service.Result;

/**
 * 活动状态；审核拒绝
 */
public class RefuseState implements State {

//	@Override
//	public Result arraignment(ActivityInfo info) {
//		return new Result(ERROR_CODE, "已审核状态不可重复提审");
//	}
//
//	@Override
//	public Result checkPass(ActivityInfo info) {
//		return new Result(ERROR_CODE, "已审核状态不可重复审核");
//	}

	@Override
	public Result checkRefuse(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Refuse);
		return new Result(ERROR_CODE, "不可重复审核拒绝");
	}

	@Override
	public Result checkRevoke(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Check);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 待审核(审核拒绝状态撤销)");
	}

	@Override
	public Result close(ActivityInfo info) {
		info.setStatusEnum(StatusEnum.Close);
		return new Result(SUCCESS_CODE, "活动状态变更完成：当前状态 -> 已关闭");
	}

//	@Override
//	public Result open(ActivityInfo info) {
//		return new Result(ERROR_CODE, "非关闭活动不可开启");
//	}
//
//	@Override
//	public Result doing(ActivityInfo info) {
//		return new Result(ERROR_CODE, "审核拒绝不可执行活动为进行中");
//	}

}
