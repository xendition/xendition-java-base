package com.michael.demos.base.design.mediator.database;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *   Elasticsearch
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/21 17:41
 */
public class EsDatabase extends AbstractDatabase {

	private List<String> dataset = new ArrayList<String>();

	public EsDatabase(AbstractMediator mediator) {
		super(mediator);
	}

	@Override
	public void addData(String data) {
		System.out.println("Elasticsearch 添加数据：" + data);
		this.dataset.add(data);
	}

	@Override
	public void add(String data) {
		addData(data);
		// 数据同步作业交给中介者管理
		this.mediator.sync(DatabaseEnum.Elasticsearch, data);
	}

	public void count() {
		int count = this.dataset.size();
		System.out.println("Elasticsearch 统计，目前有 " + count + " 条数据，数据：" + this.dataset.toString());
	}
}
