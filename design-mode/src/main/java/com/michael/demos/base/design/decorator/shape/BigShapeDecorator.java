package com.michael.demos.base.design.decorator.shape;

/**
 * 类功能描述:
 * <pre>
 *   扩展了 ShapeDecorator 类的实体装饰类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 17:12
 */
public class BigShapeDecorator extends ShapeDecorator {

    public BigShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    @Override
    public void draw() {
        decoratedShape.draw();
        // 方法增强
        setSize(decoratedShape);
    }

    private void setSize(Shape decoratedShape) {
        System.out.println("变大了");
    }
}
