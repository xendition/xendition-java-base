package com.michael.demos.base.design.composite.decision.service.logic;

import com.michael.demos.base.design.composite.decision.model.vo.TreeNodeLinkVO;

import java.util.List;
import java.util.Map;

/**
 * 类功能描述:
 * <pre>
 *   树节点逻辑过滤器器接⼝口
 *   这一部分定义了了适配的通⽤用接口，逻辑决策器器、获取决策值，
 *   让每一个提供决策能⼒力力的节点都必须实现此接口，保证统一性。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/27 15:13
 */
public interface LogicFilter {

    /**
     * 逻辑决策器器
     *
     * @param matterValue          决策值
     * @param treeNodeLineInfoList 决策节点
     *
     * @return 下一个节点Id
     */
    Long filter(String matterValue, List<TreeNodeLinkVO> treeNodeLineInfoList);

    /**
     * 获取决策值
     *
     * @param decisionMatter 决策物料料
     *
     * @return 决策值
     */
    String matterValue(Long treeId, String userId, Map<String, String> decisionMatter);
}
