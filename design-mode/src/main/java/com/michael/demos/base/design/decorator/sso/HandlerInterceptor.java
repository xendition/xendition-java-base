package com.michael.demos.base.design.decorator.sso;

public interface HandlerInterceptor {

    boolean preHandle(String request, String response, Object handler);

}
