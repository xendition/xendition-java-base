package com.michael.demos.base.design.memento.configrollback;

import java.util.Date;

/**
 * 类功能描述:
 * <pre>
 *   xxxx
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:46
 */
public class Client {

	public static void main(String[] args) {

		Admin admin = new Admin();

		ConfigOriginator configOriginator = new ConfigOriginator();

		configOriginator.setConfigFile(new ConfigFile("1000001", "配置内容A=哈哈", new Date(), "Michael"));
		admin.append(configOriginator.saveMemento()); // 保存配置

		configOriginator.setConfigFile(new ConfigFile("1000002", "配置内容A=嘻嘻", new Date(), "Michael"));
		admin.append(configOriginator.saveMemento()); // 保存配置

		configOriginator.setConfigFile(new ConfigFile("1000003", "配置内容A=么么", new Date(), "Michael"));
		admin.append(configOriginator.saveMemento()); // 保存配置

		configOriginator.setConfigFile(new ConfigFile("1000004", "配置内容A=嘿嘿", new Date(), "Michael"));
		admin.append(configOriginator.saveMemento()); // 保存配置

		// 历史配置(回滚)
		configOriginator.getMemento(admin.undo());
		System.out.println("历史配置(回滚)undo：" + configOriginator.getConfigFile());

		// 历史配置(回滚)
		configOriginator.getMemento(admin.undo());
		System.out.println("历史配置(回滚)undo：" + configOriginator.getConfigFile());

		// 历史配置(前进)
		configOriginator.getMemento(admin.redo());
		System.out.println("历史配置(前进)redo：" + configOriginator.getConfigFile());

		// 历史配置(获取)
		configOriginator.getMemento(admin.get("1000002"));
		System.out.println("历史配置(获取)get：" + configOriginator.getConfigFile());
	}
}
