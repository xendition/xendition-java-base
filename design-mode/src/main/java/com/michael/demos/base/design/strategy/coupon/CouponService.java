package com.michael.demos.base.design.strategy.coupon;

import java.math.BigDecimal;

/**
 * 类功能描述:
 * <pre>
 *   策略模式 —— 优惠券服务类
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/12/30 16:08
 */
public class CouponService {

    private ICouponDiscountStrategy couponDiscountStrategy;

    public CouponService(ICouponDiscountStrategy couponDiscountStrategy) {
        this.couponDiscountStrategy = couponDiscountStrategy;
    }

    public BigDecimal discountAmount(BigDecimal orderPrice, BigDecimal couponPrice) {
        return couponDiscountStrategy.discountAmount(orderPrice, couponPrice);
    }
}
