package com.michael.demos.base.design.memento.multi;

import java.util.ArrayList;
import java.util.List;

/**
 * 类功能描述:
 * <pre>
 *  多检查点实现备忘录 —— 负责人角色类
 * 	负责人角色有如下责任：
 * 		1. 负责保存备忘录对象。
 * 		2. 不检查备忘录对象的内容
 *
 * 	负责人角色类负责保存备忘录对象，但是从不修改（甚至不查看）备忘录对象的内容。
 * </pre>
 *
 * @author Michael
 * @version 1.0
 * @date 2020/8/24 15:17
 */
public class Caretaker {
	private Originator originator;
	private List<Memento> mementos = new ArrayList<>();
	private int current;

	/**
	 * 构造函数
	 *
	 * @param originator
	 */
	public Caretaker(Originator originator) {
		this.originator = originator;
		this.current = 0;
	}

	/**
	 * 创建一个新的检查点
	 *
	 * @return
	 */
	public int createMemento() {
		Memento memento = this.originator.createMemento();
		this.mementos.add(memento);
		return this.current++;
	}

	/**
	 * 将发起人对象状态恢复到某一个检查点
	 *
	 * @param index
	 */
	public void restoreMemento(int index) {
		Memento memento = mementos.get(index);
		originator.restoreMemento(memento);
	}

	/**
	 * 将某一个检查点删除
	 *
	 * @param index
	 */
	public void removeMemento(int index) {
		mementos.remove(index);
	}

	public void printAll() {
		for (int i = 0; i < mementos.size(); i++) {
			System.out.println("index i : " + i + " : " + mementos.get(i) + " : " + mementos.get(i).getStates());
			System.out.println("---------------------------------");
		}
	}
}
