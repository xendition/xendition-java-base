
[TOC]

# IOC
## 用户配置 bean 定义
1. XML
```xml 
    <bean id="abean" class="com.study.spring.samples.ABean">
        <constructor-arg type="String" value="abean01"></constructor-arg>
        <constructor-arg ref="cbean"></constructor-arg>
    </bean>
    <bean id="cbean" class="com.study.spring.samples.CBean">
        <constructor-arg type="String" value="cbean01"></constructor-arg>
    </bean>
```
2. 注解方式
```java 
    package com.study.spring.samples;
    
    import com.study.spring.context.config.annotation.Autowired;
    import com.study.spring.context.config.annotation.Component;
    import com.study.spring.context.config.annotation.Qualifier;
    import com.study.spring.context.config.annotation.Value;
    
    @Component(initMethodName = "init", destroyMethodName = "destroy")
    public class ABean {
    
        private String name;
    
        private CBean cb;
    
        @Autowired
        private DBean dbean;
    
        @Autowired
        public ABean(@Value("leesmall") String name, @Qualifier("cbean01") CBean cb) {
            super();
            this.name = name;
            this.cb = cb;
            System.out.println("调用了含有CBean参数的构造方法");
        }
    
        public ABean(String name, CCBean cb) {
            super();
            this.name = name;
            this.cb = cb;
            System.out.println("调用了含有CCBean参数的构造方法");
        }
    
        public ABean(CBean cb) {
            super();
            this.cb = cb;
        }
    }
```
3. Java-based容器配置方式：
```java
    @Configuration
    public class AppConfig {
        @Bean
        public MyService myService() {
            return new MyServiceImpl();
        }
    }
```
## IOC容器加载bean定义
1. XML
```java 
    //类路径下加载xml配置文件创建bean定义
    ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");
    
    //文件系统下加载xml配置文件创建bean定义
    ApplicationContext context1 = new FileSystemXmlApplicationContext("e:/study/application.xml");
    
    //通用的xml方式加载xml配置文件创建bean定义
    ApplicationContext context3 = new GenericXmlApplicationContext("file:e:/study/application.xml");
```
2. 注解方式
```xml 
    <beans>
        <context:component-scan base-package="com.study" />
    </beans>
```

```java 
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.ComponentScan;
    import org.springframework.context.annotation.Configuration;
    
    @Configuration
    @ComponentScan(basePackages="com.study")
    public class AppConfig {
    
        @Bean
        public MyService myService() {
            return new MyServiceImpl();
        }
    
    }
```

```java 
    // 扫描注解的方式创建bean定义
    ApplicationContext ctx= new AnnotationConfigApplicationContext();
    ctx.scan("com.study");
    ctx.refresh();
    MyService myService = ctx.getBean(MyService.class);
```
3. Java-based容器配置

```java 
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        MyService myService = ctx.getBean(MyService.class);
        myService.doStuff();
    }
```

```java 
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(
            MyServiceImpl.class, Dependency1.class, Dependency2.class
        );
        MyService myService = ctx.getBean(MyService.class);
        myService.doStuff();
    }
```

```java 
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AppConfig.class, OtherConfig.class);
        ctx.register(AdditionalConfig.class);
        ctx.refresh();
        MyService myService = ctx.getBean(MyService.class);
        myService.doStuff();
    }
```
```java
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.ComponentScan;
    import org.springframework.context.annotation.Configuration;
    
    @Configuration
    @ComponentScan(basePackages="com.study")
    public class AppConfig {
    
        @Bean
        public MyService myService() {
            return new MyServiceImpl();
        }
    
    }
```

```java 
public static void main(String[] args) {
    ApplicationContext ctx = new AnnotationConfigApplicationContext();
    ctx.scan("com.study");
    ctx.refresh();
    MyService myService = ctx.getBean(MyService.class);
}
```
## IOC容器创建bean实例
## 用户从IOC容器获取bean定义
## 核心方法 AbstractApplicationContext#refresh
```java 
public void refresh() throws BeansException, IllegalStateException {
    synchronized (this.startupShutdownMonitor) {
        // Prepare this context for refreshing.
        // STEP 1： 刷新预处理 设置flag、时间，初始化properties等
        prepareRefresh();

        // Tell the subclass to refresh the internal bean factory.
        // STEP 2：获取ApplicationContext中组合的BeanFactory
            // a） 创建IoC容器（DefaultListableBeanFactory）
            // b） 加载解析XML文件（最终存储到Document对象中）
            // c） 读取Document对象，并完成BeanDefinition的加载和注册工作
        ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();

        // Prepare the bean factory for use in this context.
        // STEP 3： 对IoC容器进行一些预处理（设置一些公共属性） 设置类加载器，添加后置处理器等准备
        prepareBeanFactory(beanFactory);

        try {
            // Allows post-processing of the bean factory in context subclasses.
            // STEP 4：供子类实现的后置处理
            postProcessBeanFactory(beanFactory);

            // Invoke factory processors registered as beans in thecontext.
            // STEP 5： 调用 BeanFactoryPostProcessor 后置处理器对 BeanDefinition处理
            invokeBeanFactoryPostProcessors(beanFactory);

            // Register bean processors that intercept bean creation.
            // STEP 6： 注册 BeanPostProcessor 后置处理器
            registerBeanPostProcessors(beanFactory);

            // Initialize message source for this context.
            // STEP 7： 初始化一些消息源（比如处理国际化的i18n等消息源）
            initMessageSource();

            // Initialize event multicaster for this context.
            // STEP 8： 初始化应用事件广播器
            initApplicationEventMulticaster();

            // Initialize other special beans in specific context subclasses.
            // STEP 9： 初始化一些特殊的bean
            onRefresh();

            // Check for listener beans and register them.
            // STEP 10： 注册一些监听器
            registerListeners();

            // Instantiate all remaining (non-lazy-init) singletons.
            // STEP 11： 实例化剩余的单例bean（非懒加载方式）
            // 注意事项：Bean的IoC、DI和AOP都是发生在此步骤
            finishBeanFactoryInitialization(beanFactory);

            // Last step: publish corresponding event.
            // STEP 12： 完成刷新时，需要发布对应的事件
            finishRefresh();

        } catch (BeansException ex) {
            if (logger.isWarnEnabled()) {
                logger.warn("Exception encountered during contextinitialization - " +
                        "cancelling refresh attempt: " + ex);
            }
            // Destroy already created singletons to avoid danglingresources.
            destroyBeans();
            // Reset 'active' flag.
            cancelRefresh(ex);
            // Propagate exception to caller.
            throw ex;
        } finally {
            // Reset common introspection caches in Spring's core,since we
            // might not ever need metadata for singleton beans anymore...
            resetCommonCaches();
        }
    }
}
```
# DI
# AOP
# TX

# Spring 容器扩展点
[扩展参考1](https://juejin.im/post/6844903837656940557)   
[扩展参考2](https://www.jianshu.com/p/397c15cbf34a)

## 容器扩展点如何生效？
- 重点查看 AbstractApplicationContext#refresh
![](Spring扩展.png)

## BeanPostProcessor
- 如果你想要在 Spring 容器完成容器初始化,配置和初始化 Bean 之后实现一些定制的逻辑,你可以通过插入一个或者多个定制的 BeanPostProcessor实现。
- AOP 自动代理是基于 BeanPostProcessor 实现的

## ApplicationContextAwareProcessor
- 继承了 BeanPostProcessor
- 在一个bean初始化之前,如果这个 bean 有 Aware 接口,实现了 EnvironmentAware,EmbeddedValueResolverAware,
ResourceLoaderAware,ApplicationEventPublisherAware,MessageSourceAware,ApplicationContextAware 相关接口,
就通过这些 aware 的相关接口将上下文设置进去

## InstantiationAwareBeanPostProcessor
- 继承了 BeanPostProcessor
- 用于在实例化之后,但在设置显式属性或自动装配之前,设置实例化之前的回调函数
- 通常用于抑制特定目标bean的默认实例化
- 这个接口是一个专用接口,主要用于框架内的内部使用

## InitializingBean
- 在执行完 BeanPostProcessor 的 postProcessBeforeInitialization 方法后,
如果这个bean实现了 InitializingBean 接口,则会去调用 afterPropertiesSet 方法。


## BeanFactoryPostProcessor
- BeanPostProcessor 能处理的事情它也能处理,但是不建议 **(违背了标准的容器生命周期)**
- 与 BeanPostProcessor 类似,主要的不同在于：BeanFactoryPostProcessor 操作于 Bean 的配置元数据
- 示例：类名替换 PropertyPlaceholderConfigurer (bean 工厂后置处理器)
- 示例：PropertyOverrideConfigurer (bean 工厂后置处理器)

## BeanDefinitionRegistryPostProcessor
- 继承了 BeanFactoryPostProcessor 接口
- Mybatis中org.mybatis.spring.mapper.MapperScannerConfigurer就实现了该方法
```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

public class DefaultBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        logger.info("BeanDefinitionRegistryPostProcessor的postProcessBeanDefinitionRegistry方法,在这里可以增加修改删除bean的定义");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        logger.info("BeanDefinitionRegistryPostProcessor的postProcessBeanFactory方法,在这里可以对beanFactory做一些操作");
    }
}
```
- postProcessBeanDefinitionRegistry方法可以修改在BeanDefinitionRegistry接口实现类中注册的任意BeanDefinition

## FactoryBean
- FactoryBean 接口对 Spring IoC 容器实例化逻辑实现是可插拔的。
如果你有复杂的初始化代码,使用 Java 代码 好于冗长的 XML 配置,你可以创建自己的 FactoryBean,在这个类里写复杂的实例化,
并且将定制的 FactoryBean 插入到容器中。

## Aware 接口的子接口
- 在在执行完 BeanPostProcessor 的 postProcessBeforeInitialization 方法前,
如果bean实现了 BeanNameAware 或 BeanClassLoaderAware 或 BeanFactoryAware,
则会调用接口相关的方法,入参就是这个bean关心的值。

## ApplicationListener
```java
package com.study.spring.ext;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

//如果你想对容器工作过程中发生的节点事件进行一些处理，比如容器要刷新、容器要关闭了，那么你就可以实现ApplicationListener
@Component
public class MyApplicationListener implements ApplicationListener<ApplicationEvent> {

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("-----收到应用事件：" + event);
    }
}
```

# Spring 关键类注释说明
## org.springframework.beans.factory.BeanFactory
- Spring bean 容器的 root 接口，bean容器的基本视图
- ListableBeanFactory和ConfigurableBeanFactory，可用于特定的目的。
- 根据bean的定义，工厂将返回一个被包含对象的独立实例(原型设计模式)或者一个单独的共享实例
- 返回哪种类型的实例取决于bean工厂配置:API是相同的
- 进一步的 scope 取决于具体的 application context
- BeanFactory是应用程序组件的中央注册中心，并集中配置应用程序组件
- 通常最好依靠依赖项注入(setters 或者 constructors)来配置应用程序对象(推模式)
- Spring的依赖注入(DI)功能是使用这个BeanFactory接口及其子接口实现的。
- BeanFactory将加载存储在配置源(如XML文档)中的bean定义，并使用org.springframework.bean包来配置bean。
- 但是，实现可以直接在Java代码中返回它所创建的Java对象。对于如何存储定义没有限制
- 与ListableBeanFactory中的方法相比，此接口中的所有操作还将检查父工厂是否为HierarchicalBeanFactory。
- 如果在这个工厂实例中没有找到bean，就会询问直接的父工厂。这个工厂实例中的bean应该覆盖任何父工厂中同名的bean。
- Bean工厂实现应该尽可能地支持标准Bean生命周期接口。
- BeanFactory 初始化生命周期如下
> 1. BeanNameAware#setBeanName
> 2. BeanClassLoaderAware#setBeanClassLoader
> 3. BeanFactoryAware#setBeanFactory
> 4. EnvironmentAware#setEnvironment
> 5. EmbeddedValueResolverAware#setEmbeddedValueResolver
> 6. ResourceLoaderAware#setResourceLoader (仅适用于在应用程序上下文中运行时)
> 7. ApplicationEventPublisherAware#setApplicationEventPublisher (仅适用于在应用程序上下文中运行时)
> 8. MessageSourceAware#setMessageSource (仅适用于在应用程序上下文中运行时)
> 9. ApplicationContextAware#setApplicationContext (仅适用于在应用程序上下文中运行时)
> 10. ServletContextAware#setServletContext (仅适用于在web应用程序上下文中运行时)
> 11. BeanPostProcessors#postProcessBeforeInitialization
> 12. InitializingBean#afterPropertiesSet
> 13. 自定义 init 方法
> 14. BeanPostProcessors#postProcessAfterInitialization

- BeanFactory 关闭生命周期如下
> 1. DestructionAwareBeanPostProcessors#postProcessBeforeDestruction
> 2. DisposableBean#destroy
> 3. 自定义 destroy 方法

## org.springframework.beans.factory.config.BeanDefinition
- BeanDefinition描述一个bean实例，该实例具有属性值、构造函数参数值和由具体实现提供的进一步信息。
- 这只是一个最小的接口:主要目的是允许BeanFactoryPostProcessor(如PropertyPlaceholderConfigurer)自省和修改属性值和其他bean元数据。

## org.springframework.beans.factory.annotation.AnnotatedBeanDefinition
- 扩展了 BeanDefinition 接口
- 注解处理用

## org.springframework.beans.factory.support.BeanDefinitionRegistry
- 为持有bean定义(例如 RootBeanDefinition 和 ChildBeanDefinition 实例)的注册中心提供接口。
- 通常由内部使用AbstractBeanDefinition层次结构的BeanFactories实现。
- 这是Spring的bean工厂包中封装bean定义注册的唯一接口。
- 标准的BeanFactory接口只涉及对完全配置的工厂实例的访问。
- Spring的bean定义读者希望处理这个接口的实现。
- Spring核心中已知的实现者是 DefaultListableBeanFactory 和 GenericApplicationContext。

## org.springframework.beans.factory.config.SingletonBeanRegistry
- 该接口为共享bean实例定义注册表。
- 可以由 BeanFactory 实现实现，以便以统一的方式公开它们的单例管理功能。
- ConfigurableBeanFactory 接口扩展了这个接口。
