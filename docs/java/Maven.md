单独构建模块jsoft-web，同时会构建jsoft-web模块依赖的其他模块
`mvn install -pl jsoft-web -am`

单独构建模块jsoft-common，同时构建依赖模块jsoft-common的其他模块
`mvn install -pl jsoft-common -am -amd`
