
1. 查看firewall状态
firewall-cmd --state
2. 查看firewall服务状态
systemctl status firewalld
3. 开启firewalld服务
service firewalld start  /   systemctl start firewalld
4. 重启firewalld服务
service firewalld restart  /  systemctl restart firewalld
5. 关闭fired服务
service firewalld stop   /   systemctl stop firewalld
6. 查看防火墙规则
firewall-cmd --list-all
7. 查询端口是否开放
firewall-cmd --query-port=8080/tcp
8. 开放8080端口
firewall-cmd --permanent --add-port=8080/tcp
9. 移除端口
firewall-cmd --permanent --remove-port=8080/tcp
10. 重启防火墙
firewall-cmd --reload
11. 禁止firewall开机启动
systemctl disable firewalld
