

### Docker 简介

#### 官网
https://www.docker.com/

![](.Docker说明_images/b2a84adf.png)

#### 相关概念
```text
1. 容器是一种标准化软件单元，Docker创建了容器的行业标准(着重于简单性，健壮性，可移植性)
   它包括运行应用程序所需的一切:代码、运行时、系统工具、系统库和设置。
2. Image 是一个轻量级的、独立的、可执行的软件包。在运行时成为容器
3. 容器之间默认隔离，确保了程序的安全性
4. 容器虚拟化了操作系统，相比虚拟机(虚拟了硬件)更加便携与高效
```

### Docker 安装

建议使用Linux系统安装

[virtualbox下载地址](https://download.virtualbox.org/virtualbox)

[Docker官方文档](https://docs.docker.com/get-docker/)

[Docker-ce For Windows10 Download](https://hub.docker.com/editions/community/docker-ce-desktop-windows)

```shell
# 停止docker服务
systemctl stop docker
# 查看当前版本
rpm -qa | grep docker
# 卸载软件包
yum erase docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine \
                  docker-ce
# 关配置文件
find /etc/systemd -name '*docker*' -exec rm -f {} \;
find /etc/systemd -name '*docker*' -exec rm -f {} \;
find /lib/systemd -name '*docker*' -exec rm -f {} \;
rm -rf /var/lib/docker   #删除以前已有的镜像和容器,非必要
rm -rf /var/run/docker 

# 安装新版本

# 软件包安装
yum install -y yum-utils  device-mapper-persistent-data lvm2

# 添加yum源
yum-config-manager \
--add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
# 查看可安装的版本
yum list docker-ce --showduplicates | sort -r

# 安装最新版本
yum install docker-ce -y

# 启动并开机自启
systemctl start docker
systemctl enable docker


```

### Docker 配置

### 常用命令
run  
`docker run hello-world`


### Docker-compose
https://www.cnblogs.com/sanduzxcvbnm/p/13186899.html
