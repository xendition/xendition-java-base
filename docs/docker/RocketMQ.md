
## 拉取镜像
`docker pull rocketmqinc/rocketmq:4.4.0`

## 启动容器
```shell script
docker run -d -p 9876:9876 \
 -v {RmHome}/data/namesrv/logs:/root/logs \
 -v {RmHome}/data/namesrv/store:/root/store \
 --name rmqnamesrv -e "MAXPOSSIBLE_HEAP=100000000" rocketmqinc/rocketmq:4.4.0 sh mqnamesrv
```

#### 注意事项
1. {RmHome} 要替换成你的宿主机想保存 MQ 的日志与数据的地方
2. 通过 docker 的 -v 参数使用 volume 功能，把你本地的目录映射到容器内的目录上
3. 否则所有数据都默认保存在容器运行时的内存中，重启之后就又回到最初的起点

## 安装 broker 服务器
1. 拉取镜像 `docker pull rocketmqinc/rocketmq:4.4.0`
2. 创建 broker.conf 文件
```text
brokerClusterName = DefaultCluster
brokerName = broker-a
brokerId = 0
deleteWhen = 04
fileReservedTime = 48
brokerRole = ASYNC_MASTER
flushDiskType = ASYNC_FLUSH
brokerIP1 = {本地外网 IP}
```
3. 启动
```shell script
docker run -d -p 10911:10911 -p 10909:10909 \
 -v  {RmHome}/data/broker/logs:/root/logs \
 -v  {RmHome}/rocketmq/data/broker/store:/root/store \
 -v  {RmHome}/conf/broker.conf:/opt/rocketmq-4.4.0/conf/broker.conf \
 --name rmqbroker --link rmqnamesrv:namesrv \
 -e "NAMESRV_ADDR=namesrv:9876" \
 -e "MAX_POSSIBLE_HEAP=200000000" rocketmqinc/rocketmq:4.4.0 sh mqbroker -c /opt/rocketmq-4.4.0/conf/broker.conf
```
## 安装 rocketmq 控制台
1. 拉取镜像 `docker pull pangliang/rocketmq-console-ng`
2. 启动
`docker run --name rmqconsole -d -p 8080:8080 --link rmqnamesrv:namesrv -e "JAVA_OPTS=-Drocketmq.namesrv.addr=namesrv:9876"  pangliang/rocketmq-console-ng`

## 开机启动
`docker update --restart always rmqnamesrv`  
`docker update --restart always rmqbroker`  
`docker update --restart always rmqconsole`  

## 不开机启动
`docker update --restart no rmqnamesrv`  
`docker update --restart no rmqbroker`  
`docker update --restart no rmqconsole`  

### restart 参数值选择
1. no  不自动重启容器. (默认值)
2. on-failure  容器发生error而退出(容器退出状态不为0)重启容器,可以指定重启的最大次数，如：on-failure:10
3. unless-stopped  在容器已经stop掉或Docker stoped/restarted的时候才重启容器
4. always  在容器已经stop掉或Docker stoped/restarted的时候才重启容器，手动stop的不算


cmd startup.cmd -m standalone
