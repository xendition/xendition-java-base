```shell

# 拉包
docker pull gitlab/gitlab-ce
# 运行
docker run -d -p 8443:443 -p 2001:80 -p 2022:22 \
  --name gitlab \
  --restart always \
  -v /usr/local/app/gitlab/config:/etc/gitlab:Z \
  -v /usr/local/app/gitlab/logs:/var/log/gitlab:Z \
  -v /usr/local/app/gitlab/data:/var/opt/gitlab:Z \
  gitlab/gitlab-ce

vim /usr/local/app/gitlab/config/gitlab.rb
# 修改配置
external_url 'http://gitlab.20sticks.com'
gitlab_rails['gitlab_shell_ssh_port'] = 2022

## 查看配置
cat /usr/local/app/gitlab/config/gitlab.rb | grep -v "#" | grep -v "^$"

# nginx配置 tcp 代理

# 增加此配置 http 外面 
stream {
upstream ssh {
        hash $remote_addr consistent;
        server  172.16.32.7:2022 max_fails=3 fail_timeout=10s;
    }
    server {
        listen 2022;
        proxy_connect_timeout 20s;
        proxy_timeout 5m;
        proxy_pass ssh;
    }
}

http{}


    upstream gitlab {
        server 172.18.153.41:1234 fail_timeout=0;
    }

    server {
        listen 443 ssl;
        server_name gitlab.12kedu.fun;
        ssl_certificate /etc/nginx/cert/gitlab/gitlab.12kedu.fun.pem;
        ssl_certificate_key /etc/nginx/cert/gitlab/gitlab.12kedu.fun.key;
        location ~ ^/(.*){
            proxy_pass http://gitlab;
            proxy_set_header REMOTE_ADDR $remote_addr;
            proxy_set_header Host $http_host;
            proxy_http_version 1.1;
            proxy_set_header Connection "";
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
    }

    server {
        listen 80;
        server_name gitlab.12kedu.fun;
 
        location ^~ /.well-known/acme-challenge/ {
            alias /data/sites/challenges/;
            try_files $uri = 404;
        }
 
        location / {
            rewrite ^/(.*)$ https://$host/$1 permanent;
        }
    }



## 进入容器
docker exec -it xx bash
## 重载配置
gitlab-ctl reconfigure
## 重启
gitlab-ctl restart

```
