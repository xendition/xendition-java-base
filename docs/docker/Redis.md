 
 ## 镜像
 `sudo docker pull redis`
 
 ## Redis 启动
 `sudo docker run -p 6379:6379 --name redis \
 -v /mydata/redis/data:/data \
 -v /mydata/redis/conf/redis.conf:/etc/redis/redis.conf \
 -d redis redis-server /etc/redis/redis.conf`


```shell
docker pull redis:5.0

mkdir -p /usr/local/app/redis/data
touch /usr/local/app/redis/redis.conf


docker run --name redis626 -p 6379:6379 \
-v /usr/local/app/redis/6.2.6/redis.conf:/etc/redis/redis.conf \
-v /usr/local/app/redis/6.2.6/data:/data \
-d --restart=always redis:6.2.6 redis-server /etc/redis/redis.conf \
--appendonly yes \
--requirepass "root"

docker run --name redis5.0 -p 6379:6379 \
-v /usr/local/app/redis/redis.conf:/etc/redis/redis.conf \
-v /usr/local/app/redis/data:/data \
-d --restart=always redis:5.0 redis-server /etc/redis/redis.conf \
--appendonly yes \
--requirepass "root"


docker run --name redis -p 6379:6379 \
-v /usr/local/app/redis/redis.conf:/etc/redis/redis.conf \
-v /usr/local/app/redis/data:/data \
-d --restart=always redis:latest redis-server /etc/redis/redis.conf \
--appendonly yes \
--requirepass "root"


# 日志
docker logs redis
# 进入容器 
docker exec -it redis bash 

````

redis 命令

```shell
redis-cli
auth root

# String
set key value
get key

# list
lpush key value value value ...
llen key
lpop key
lrange key start end

# hash
hset key hash_key hash_value

# 检查值类型
type key

# 清空库
flushdb

```
--name redis 【容器名】
-p 6379:6379 【映射端口】
-v /usr/local/app/redis/redis.conf:/etc/redis/redis.conf 【conf文件挂载目录】
-v /usr/local/app/redis/data:/data 【data挂载目录】
-d redis:5.0 【后台运行镜像】
--restart=always 【docker重启后自动启动镜像】
redis-server /etc/redis/redis.conf 【在容器执行redis-server启动命令，执行conf文件】
--appendonly yes 【持久化】
--requirepass "root" 【设置密码】
