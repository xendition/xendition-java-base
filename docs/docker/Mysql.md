
sudo docker pull mysql:5.7

# 运行 Mysql 容器，映射目录，设置必须 Mysql 参数
sudo docker run -p 3333:3306 --name mysql \
-v /docker/mysql/config:/etc/mysql \
-v /docker/mysql/data:/var/lib/mysql \
-v /docker/mysql/logs:/var/log/mysql \
-e MYSQL_ROOT_PASSWORD=1 \
-e MYSQL_USER="Michael" \
-e MYSQL_PASSWORD="1" \
-d mysql:5.7 \
--lower_case_table_names=1 \
--max-allowed-packet=1073741824 \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_unicode_ci \
--innodb_log_file_size=256m


docker exec -it 8209a8372bf9 /bin/bash


# 运行 Mysql 容器，映射目录，设置必须 Mysql 参数
$ docker run -d -p 3306:3306 --name mysql \
-v /home/mysql/mysql/conf:/etc/mysql \
-v /home/mysql/mysql/logs:/var/log/mysql \
-v /home/mysql/mysql/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=daodaotest \
mysql:5.7 \
--lower_case_table_names=1 \
--max-allowed-packet=1073741824 \
--character_set_server=utf8 \
--innodb_log_file_size=256m
