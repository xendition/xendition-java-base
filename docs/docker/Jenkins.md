```shell

docker pull jenkins/jenkins
docker run -d -p 3001:8080 -p 3002:50000 \
--name jenkins \
-v /usr/local/app/jenkins/jenkins_home:/var/jenkins_home \
-v /etc/localtime:/etc/localtime \
jenkins/jenkins


docker run -d -p 3001:8080 -p 3002:50000 \
--name jenkins \
-v /usr/local/app/jenkins/home:/var/jenkins_home \
-v /usr/local/app/jenkins/m2:/root/.m2 \
-v /etc/localtime:/etc/localtime \
myjenkins:1




# jenkins 重启
jenkins.xx.com/restart

    upstream jenkins {
        server 127.0.0.1:10000 fail_timeout=0;
    }

    server {
        listen 80;
        server_name jenkins.20sticks.com;

        location / {
            proxy_pass http://jenkins;
    
			sendfile off;
		
			proxy_set_header   Host             $host:$server_port;
			proxy_set_header   X-Real-IP        $remote_addr;
			proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
			proxy_max_temp_file_size 0;
		
			# This is the maximum upload size
			client_max_body_size       10m;
			client_body_buffer_size    128k;
		
			proxy_connect_timeout      90;
			proxy_send_timeout         90;
			proxy_read_timeout         90;
		
			proxy_temp_file_write_size 64k;
		
			proxy_http_version 1.1;
			proxy_request_buffering off;
			proxy_buffering off; 
        }
    }

```

[jenkins插件下载地址](http://updates.jenkins-ci.org/download/plugins/)

http://www.babyitellyou.com/details?id=60669ef20a6c6440e0560f35

https://zhuanlan.zhihu.com/p/89312003
