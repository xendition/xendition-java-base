package com.michael.demos.base.base.java.datastructure;

import java.util.ArrayList;
import java.util.List;

/**
 * JAVA 常用非基础数据类型
 *
 * @author Michael
 */
public class CommonDataTypeDemo {

    public static void main(String[] args) {

//        theString();

//        TODO 本机测试未达到效果
//        internTest();
//        internTest2();
    }

    public static void theString() {
        System.out.println("最常用的数据类型：String");
        System.out.println("空字符串占用内存(40Byte)：对象头（8 字节）+ 引用 (4 字节 )  + char 数组（16 字节）+ 1个 int（4字节）+ 1个long（8字节）= 40 字节");

        // 指向常量池
        String s1 = "Michael";
        // 指向堆
        String s2 = new String("Michael");

        String s3 = String.valueOf(3.14159265357989);
        System.out.println("字符串实际存储位置在字符串学量池中");

        // 常量池中已经有了，直接使用常量池中的
        String s4 = s2.intern();
        // 新的对象，指向堆中新的位置
        String s5 = new String("Michael");

        // 常量池中已经有了，直接使用常量池中的
        String s6 = s5.intern();

        System.out.println("s1==s2:" + (s1 == s2));
        System.out.println("s1==s4:" + (s1 == s4));
        System.out.println("s1==s5:" + (s1 == s5));
        System.out.println("s1==s6:" + (s1 == s6));
        System.out.println("s2==s4:" + (s2 == s4));
        System.out.println("s2==s5:" + (s2 == s5));
        System.out.println("s2==s6:" + (s2 == s6));
        System.out.println("s4==s5:" + (s4 == s5));
        System.out.println("s4==s6:" + (s4 == s6));
        System.out.println("s6==s5:" + (s6 == s5));

        // s1 s4 s6 均指向常量池
        // s2 s5 均指向堆，且位置不同
    }

    /**
     * String的intern测试 —— 对照组：未使用intern
     * TODO 需要配置JVM参数 -Xms5m -Xmx5m
     */
    public static void internTest() {

        String bigString = "xvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFz";
        List<User> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            User u = new User();
            u.setName(new String(bigString));
            list.add(u);
            System.out.println(i);
        }
        System.out.println("SUCCESS!");
    }

    /**
     * String的intern测试 —— 实验组：使用intern
     * TODO 需要配置JVM参数 -Xms5m -Xmx5m
     */
    public static void internTest2() {

        String bigString = "xvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFzxvZw3mQdnI_*Ycqpu_@njDfmpGuDXzd8RaA2fUEUk6XcTa9Sl0@vWO98cJRBipKaE(+$vGNI6B3CGYP%v6gmSWoU9CqLjxxZl)%_DQJc#BQMC(5t&MdYx7Q90aPxQnjGhZqOUdJ4pqaw8M3CzLu!dVAtXZk^gtuOWpBAgGOmG$2xCQ&X(fw%sUnABEc93Q7GG+baBg~J5T261j3Ain&yP1WmBC!+3(7_4OhiVIBtcKYW_FTAyWk~sT~gZThEcV!1UtY@gT0yKaPYeNKb(8ENc%pH2jyHkZWy^N~c+Nn#S6&eNbW2Jvj9tG_w8MPbJuD(Ac!f$zWcSQKSNH8Tma&N~ZFD1USIh1SDX!#B(ELxBYYUCzhwrJ6Splh8ppbAPW*Jmo87!)iWl$JnOYPzNueDifg%#3YZ!JNCxofLwC^OX&w1HB)RQy!aF904xc^@vKL+iFYEjePzr2dO8kK#i7DU^#f~P$(dRc44Z*+6ZKvgsCz432Ukh0jeZcG)0)P3%yFz";
        List<User2> list = new ArrayList<>();
        for (int i = 0; i < 100000; i++) {
            User2 u = new User2();
            u.setName(new String(bigString));
            list.add(u);
            System.out.println(i);
        }
        System.out.println("SUCCESS!");
    }
}

class User {
    private String name;

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class User2 {
    private String name;

    public User2() {
    }

    public User2(String name) {
        this.name = name.intern();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.intern();
    }
}