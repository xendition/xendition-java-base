package com.michael.demos.base.base.convert;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Michael
 */
@Data
public class RoleVO {
    private String roleId;
    private String roleName;
    private LocalDateTime createTime;
}
