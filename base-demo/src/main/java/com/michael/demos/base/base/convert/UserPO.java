package com.michael.demos.base.base.convert;

import lombok.Data;

/**
 * @author Michael
 */
@Data
public class UserPO {

    private Long id;
    private String name;
    private String phone;
    private String title;
    private Integer age;
    private String idCard;


}
