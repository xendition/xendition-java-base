package com.michael.demos.base.base.convert;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Michael
 */
public class UserConverter {

    public static UserDTO convertToUserDTO(UserVO item) {
        if (item == null) {
            return null;
        }
        UserDTO result = new UserDTO();
        result.setName(item.getName());
        result.setEntryDate(item.getEntryDate());
        result.setUserId(item.getUserId());
        List<RoleVO> roleList = item.getRoleList();
        if (roleList == null) {
            result.setRoleList(null);
        } else {
            result.setRoleList(roleList.stream().map(UserConverter::convertToRoleDTO).collect(Collectors.toList()));
        }
        result.setRoom(convertToRoomDTO(item.getRoom()));
        return result;
    }

    public static UserVO convertToUserVO(UserDTO item) {
        if (item == null) {
            return null;
        }
        UserVO result = new UserVO();
        result.setName(item.getName());
        result.setEntryDate(item.getEntryDate());
        result.setUserId(item.getUserId());
        List<RoleDTO> roleList = item.getRoleList();
        if (roleList == null) {
            result.setRoleList(null);
        } else {
            result.setRoleList(roleList.stream().map(UserConverter::convertToRoleVO).collect(Collectors.toList()));
        }
        result.setRoom(convertToRoomVO(item.getRoom()));
        return result;
    }

    public static RoomDTO convertToRoomDTO(RoomVO item) {
        if (item == null) {
            return null;
        }
        RoomDTO result = new RoomDTO();
        result.setRoomId(item.getRoomId());
        result.setBuildingId(item.getBuildingId());
        return result;
    }

    public static RoomVO convertToRoomVO(RoomDTO item) {
        if (item == null) {
            return null;
        }
        RoomVO result = new RoomVO();
        result.setRoomId(item.getRoomId());
        result.setBuildingId(item.getBuildingId());
//        result.setRoomName();
//        result.setBuildingName();
        return result;
    }

    public static RoleDTO convertToRoleDTO(RoleVO item) {
        if (item == null) {
            return null;
        }
        RoleDTO result = new RoleDTO();
        result.setRoleId(item.getRoleId());
        result.setRoleName(item.getRoleName());
        result.setCreateTime(item.getCreateTime());
        return result;
    }

    public static RoleVO convertToRoleVO(RoleDTO item) {
        if (item == null) {
            return null;
        }
        RoleVO result = new RoleVO();
        result.setRoleId(item.getRoleId());
        result.setRoleName(item.getRoleName());
        result.setCreateTime(item.getCreateTime());


        return result;
    }
}
