package com.michael.demos.base.base.convert;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author Michael
 */
@Data
public class UserDTO {
    private String name;
    private Date entryDate;
    private String userId;
    private List<RoleDTO> roleList;
    private RoomDTO room;
}
