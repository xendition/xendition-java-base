package com.michael.demos.base.base.convert;

import lombok.Data;

/**
 * @author Michael
 */
@Data
public class RoomDTO {
    private String roomId;
    private String buildingId;
}
