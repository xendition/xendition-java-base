package com.michael.demos.base.base.convert;

import lombok.Data;

/**
 * @author Michael
 */
@Data
public class RoomVO {
    private String roomId;
    private String buildingId;
    private String roomName;
    private String buildingName;
}
