package com.michael.demos.base.base.java8.lambda;

import java.security.SecureRandom;
import java.text.Collator;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Lambda 表达式的使用
 * <pre>
 *     Lambda 作用：
 *      1. 替代匿名内部类，减少代码量
 *      2. 集合的迭代(forEach)
 *      3. 配合Stream使用，如过滤(filter)、映射(map)、截断(limit)、去重(distinct)、统计(count)、结果收集(collect)
 *      4. 配合函数式接口Predicate实现自定义过滤规则
 *      5. 配合Optional处理null值
 *
 * </pre>
 *
 * @author Michael
 */
public class LambdaDemo1 {

    /**
     * 什么是函数式接口? 如果一个接口只有一个接口方法，那这个接口就是函数式接口，一般会声明 @FunctionalInterface 避免其变为非函数式接口
     * 什么是Lambda? 将函数式接口的实现方法块做一个简化，就是Lambda表达式，它可以直接赋值给一个变量。
     */
    public static void main(String[] args) {

        // 无参无返回值
        Runnable runnable = () -> System.out.println("Michael");
        new Thread(runnable).start();

        // 有参无返回值
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);

//        Consumer<Integer> consumer = t -> System.out.println(t) ;
//        Consumer<Integer> consumer = System.out::println;
//        list.forEach(consumer);

        list.forEach(System.out::println);

        // 有参有返回值
        System.out.println(invokeCalculator(111, 222, Integer::sum));
        System.out.println(invokeCalculator(111, 222, Integer::min));

        // 无参有返回值
        Stream<Integer> stream = Stream.generate(() -> new SecureRandom().nextInt(100));
        stream.forEach(System.out::println);

    }

    public static int invokeCalculator(int a, int b, Calculator calculator) {
        return calculator.calculate(a, b);
    }
}

@FunctionalInterface
interface Calculator {
    /**
     * 计算方法
     *
     * @param a 数
     * @param b 数
     *
     * @return 结果
     */
    int calculate(int a, int b);
}
