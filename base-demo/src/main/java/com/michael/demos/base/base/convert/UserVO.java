package com.michael.demos.base.base.convert;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import lombok.Data;

/**
 * @author Michael
 */
@Data
public class UserVO {
    private String name;
    private Date entryDate;
    private String userId;
    private List<RoleVO> roleList;
    private RoomVO room;

}
