package com.michael.demos.base.base.java.util;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Random;

/**
 * JAVA自带常用工具类
 */
public class UtilsDemo {

    @SuppressWarnings("ReplacePseudorandomGenerator")
    public static void main(String[] args) {

        // 1. 数学处理类 Math
        System.out.println(Math.addExact(123,456));

        // 2. 随机数处理 Random
        System.out.println(new Random().nextInt(100));

        // 3. 更安全的随机处理 SecureRandom
        System.out.println(new SecureRandom().nextInt(100));

        // 4. 对象工具类 Objects
        System.out.println(Objects.hashCode("Michael"));

        // 5. 数组处理类 Arrays
        Arrays.asList(1,2,3,4,5).forEach(System.out::println);

        // 6. 集合处理类 Collections
        System.out.println(Collections.max(Arrays.asList(1,2,3,4,5)));
    }
}
