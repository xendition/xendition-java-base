package com.michael.demos.base.base.convert;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Michael
 */
@Data
public class RoleDTO {
    private String roleId;
    private String roleName;
    private LocalDateTime createTime;
}
